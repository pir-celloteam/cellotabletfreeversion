
package net.pirsquare.cellotabletfree.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import net.pirsquare.cellotabletfree.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MemberViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.tv_name)
    TextView textviewName;
    @BindView(R.id.tv_phone)
    TextView textviewPhone;

    public MemberViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public TextView getTextviewName() {
        return textviewName;
    }

    public TextView getTextviewPhone() {
        return textviewPhone;
    }

}