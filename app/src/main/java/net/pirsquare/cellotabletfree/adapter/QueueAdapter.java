package net.pirsquare.cellotabletfree.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.pirsquare.cellotabletfree.R;
import net.pirsquare.cellotabletfree.adapter.holder.QueueViewHolder;
import net.pirsquare.cellotabletfree.custom.OnSingleClickListener;
import net.pirsquare.cellotabletfree.model.QueueType;
import net.pirsquare.cellotabletfree.model.RealmQueue;

import java.util.ArrayList;

/**
 * Created by kung on 10/20/16.
 */

public class QueueAdapter extends RecyclerView.Adapter<QueueViewHolder> {

    private Context mContext;
    private ArrayList<RealmQueue> listQueue;
    private QueueAdapterListener listener;

    public QueueAdapter(Context mContext, ArrayList<RealmQueue> listQueue) {
        this.mContext = mContext;
        this.listQueue = listQueue;
    }

    public void setListener(QueueAdapterListener listener) {
        this.listener = listener;
    }

    @Override
    public QueueViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater mInflater = LayoutInflater.from(parent.getContext());

        ViewGroup view = (ViewGroup) mInflater.inflate(
                R.layout.list_item_queue, parent, false);
        final QueueViewHolder holder = new QueueViewHolder(view);

        if (listener != null)
            view.setOnClickListener(new OnSingleClickListener() {
                @Override
                public void onSingleClick(View v) {
                    listener.onClickQueue(holder.getAdapterPosition());
                }
            });

        return holder;
    }

    @Override
    public void onBindViewHolder(QueueViewHolder holder, int position) {
        RealmQueue queue = listQueue.get(position);
        holder.getTextviewNumberSeat().setText(String.valueOf(queue.getNumSeat()));
        holder.getTextviewQueue().setText(String.format("%s%s", queue.getQueueGroup(), queue.getQueueIndex()));
        holder.getTextviewQueue().setTextColor(ContextCompat.getColor(mContext, queue.isActive() ? R.color.red_ci : R.color.grey_light));
        holder.getViewContainerTitle().setBackgroundColor(ContextCompat.getColor(mContext, queue.isActive() ? R.color.red_ci : R.color.grey_light));
        holder.getTextviewTimeReserve().setText(queue.getTimeReserveQueue());
        holder.getIcMobileType().setVisibility(TextUtils.isEmpty(queue.getQueueType()) ? View.GONE : (queue.getQueueType().equalsIgnoreCase(QueueType.MOBILE.getValue()) ? View.VISIBLE : View.GONE));
        holder.getIcTelephone().setVisibility(queue.getUser() == null ? View.GONE : TextUtils.isEmpty(queue.getUser().getTelephone()) || queue.getUser().getTelephone().equalsIgnoreCase("null") ? View.GONE : View.VISIBLE);
        holder.getViewBackground().setBackgroundColor(ContextCompat.getColor(mContext, !queue.isActive() ? R.color.black_40_percent : R.color.white));
        if (queue.isCancel()) {
            holder.getViewContainerTitle().setBackgroundColor(ContextCompat.getColor(mContext, R.color.grey_light));
            holder.getTextviewQueue().setTextColor(ContextCompat.getColor(mContext, R.color.white));
            holder.getViewBackground().setBackgroundColor(ContextCompat.getColor(mContext, R.color.black_40_percent));
        }

    }

    @Override
    public int getItemCount() {
        return listQueue.size();
    }

    public interface QueueAdapterListener {
        void onClickQueue(int position);
    }
}
