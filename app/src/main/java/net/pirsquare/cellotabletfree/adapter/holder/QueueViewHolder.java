package net.pirsquare.cellotabletfree.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import net.pirsquare.cellotabletfree.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class QueueViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.textview_number_seat)
    TextView textviewNumberSeat;
    @BindView(R.id.ic_telephone)
    ImageView icTelephone;
    @BindView(R.id.ic_mobile_type)
    ImageView icMobileType;
    @BindView(R.id.textview_queue)
    TextView textviewQueue;
    @BindView(R.id.textview_time_reserve)
    TextView textviewTimeReserve;
    @BindView(R.id.view_background)
    LinearLayout viewBackground;
    @BindView(R.id.container_title)
    RelativeLayout viewContainerTitle;


    public QueueViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public TextView getTextviewNumberSeat() {
        return textviewNumberSeat;
    }

    public ImageView getIcTelephone() {
        return icTelephone;
    }

    public ImageView getIcMobileType() {
        return icMobileType;
    }

    public TextView getTextviewQueue() {
        return textviewQueue;
    }

    public TextView getTextviewTimeReserve() {
        return textviewTimeReserve;
    }

    public LinearLayout getViewBackground() {
        return viewBackground;
    }

    public RelativeLayout getViewContainerTitle() {
        return viewContainerTitle;
    }
}