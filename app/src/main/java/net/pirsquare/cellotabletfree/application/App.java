package net.pirsquare.cellotabletfree.application;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.orhanobut.logger.Logger;

import net.pirsquare.cellotabletfree.R;
import net.pirsquare.cellotabletfree.manager.TabletConfig;

import net.danlew.android.joda.JodaTimeAndroid;

import org.joda.time.Chronology;
import org.joda.time.DateTimeZone;
import org.joda.time.chrono.GJChronology;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import ru.noties.spg.ContextProvider;
import ru.noties.spg.SPGManager;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by kung on 10/19/16.
 */

public class App extends Application {
    public static FirebaseAnalytics mFirebaseAnalytics;

    @Override
    public void onCreate() {
        super.onCreate();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        JodaTimeAndroid.init(this);
        Logger.init("Logger").hideThreadInfo();
        SPGManager.setContextProvider(new ContextProvider() {
            @Override
            public Context provide() {
                return App.this;
            }
        });

        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(this).build();
        if (TabletConfig.pref().isResetStructure()) {
            Log.e("isResetStructure", ">>>>>>>>>>>> 1 <<<<<<<<<<<<<");
            Realm.deleteRealm(realmConfiguration);
        }
        Realm.setDefaultConfiguration(realmConfiguration);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/RobotoCondensed-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }
}
