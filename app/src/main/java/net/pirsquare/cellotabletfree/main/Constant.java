package net.pirsquare.cellotabletfree.main;

/**
 * Created by kampolleelaporn on 3/29/16.
 */
public class Constant {

    /**
     * Release
     */
    public static final String SECRET_KEY = "cello:pirsquare.net";
    public static final String METEOR_SERVER_URL = "ws://cello-ddp-websocket-1105288242.ap-southeast-1.elb.amazonaws.com:8080/websocket";
    public static final String API_SERVER_URL = "http://ec2-54-169-250-35.ap-southeast-1.compute.amazonaws.com/tablets/v1.1/";
    public static final String API_QR_URL = "http://scan.q-happy.com/qr/";

}