package net.pirsquare.cellotabletfree.main;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.f2prateek.rx.preferences.Preference;
import com.f2prateek.rx.preferences.RxSharedPreferences;
import com.github.pwittchen.reactivenetwork.library.ReactiveNetwork;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import net.pirsquare.cellotabletfree.R;
import net.pirsquare.cellotabletfree.adapter.QueueAdapter;
import net.pirsquare.cellotabletfree.application.App;
import net.pirsquare.cellotabletfree.custom.GridAutofitLayoutManager;
import net.pirsquare.cellotabletfree.custom.OnSingleClickListener;
import net.pirsquare.cellotabletfree.custom.TabMenuButton;
import net.pirsquare.cellotabletfree.dialog.ManageQueueDialogFragmentViewController;
import net.pirsquare.cellotabletfree.dialog.ReserveQueueDialogFragmentViewController;
import net.pirsquare.cellotabletfree.manager.ConfigApplication;
import net.pirsquare.cellotabletfree.manager.DDPManager;
import net.pirsquare.cellotabletfree.manager.DateHelper;
import net.pirsquare.cellotabletfree.manager.DialogHelper;
import net.pirsquare.cellotabletfree.manager.EncodeHelper;
import net.pirsquare.cellotabletfree.manager.PrinterManager;
import net.pirsquare.cellotabletfree.manager.QueueManager;
import net.pirsquare.cellotabletfree.manager.RxBus;
import net.pirsquare.cellotabletfree.manager.TabletConfig;
import net.pirsquare.cellotabletfree.model.QueueAction;
import net.pirsquare.cellotabletfree.model.QueueState;
import net.pirsquare.cellotabletfree.model.QueueStatus;
import net.pirsquare.cellotabletfree.model.QueueType;
import net.pirsquare.cellotabletfree.model.RealmQueue;
import net.pirsquare.cellotabletfree.model.RealmUser;
import net.pirsquare.cellotabletfree.model.Service;
import net.pirsquare.cellotabletfree.service.ServiceGenerator;
import net.pirsquare.cellotabletfree.service.service.ServicesService;
import net.pirsquare.cellotabletfree.service.service.bean.ServiceParam;
import net.pirsquare.cellotabletfree.service.service.bean.ServiceResponse;
import net.pirsquare.cellotabletfree.util.GsonHelper;
import net.pirsquare.cellotabletfree.util.Utils;

import net.pirsquare.sewoo.PrintManager;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subjects.BehaviorSubject;
import rx.subscriptions.CompositeSubscription;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity implements QueueAdapter.QueueAdapterListener {

    @BindViews({R.id.tab_queue, R.id.tab_hold, R.id.tab_getin})
    List<TabMenuButton> listTab;
    @BindView(R.id.recycleview_queue)
    RecyclerView recycleviewQueue;
    @BindView(R.id.btn_reserve_queue)
    ImageView btnReserveQueue;
    //    @BindView(R.id.view_logo)
//    View viewOverlay;
    @BindView(R.id.view_logo_loading)
    RelativeLayout viewOverlay;

    @BindView(R.id.textview_server_status)
    TextView textviewServerStatus;
    @BindView(R.id.textview_printer_status)
    TextView textviewPrinterStatus;

    @BindView(R.id.textview_queue_available_reserve)
    TextView textviewQueueAvailableReserve;
    @BindView(R.id.textview_branch_name)
    TextView brachName;

    private ArrayList<RealmQueue> listQueue;
    private QueueAdapter queueAdapter;
    private GridAutofitLayoutManager gridLayoutManager;
    private String[] listTabName;
    private CompositeSubscription subscriptions;

    private CompositeSubscription observableNetwork;
    private Boolean isOnline;
    private int currentView;
    private BehaviorSubject<Boolean> observableMeteorStatus;
    private boolean isInitHardware;
    private String currentQueueManaging;
    private ManageQueueDialogFragmentViewController manageQueueDialogFragmentViewController;
    private CompositeSubscription subscriptionCurrentAvailableQueue;
    private Preference<Integer> currentAvailableQueue;
    private DatabaseReference mDatabase, currentQ;

    @Override
    protected void onResume() {
        super.onResume();

        subscriptions = new CompositeSubscription();
        subscriptions.add(RxBus.getInstance().
                toObserverable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Object>() {
                    @Override
                    public void call(Object event) {
                        if (event instanceof QueueManager.UpdateQueueCountTabMenuEvent) {
                            for (int i = 0; i < listTab.size(); i++) {
                                listTab.get(i).setNoQueue(QueueManager.getInstance().getListQueueByQueueState(QueueManager.getInstance().getState(i)).size());
                            }
                        } else if (event instanceof QueueManager.UpdateQueueChangeEvent) {
                            updateQueueInCurrentTab(currentView);
                        } else if (event instanceof PrinterManager.PrinterEvent) {
                            PrinterManager.PrinterEvent data = (PrinterManager.PrinterEvent) event;
                            textviewPrinterStatus.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), data.isConnect ? R.color.green : R.color.red_ci));
                        } else if (event instanceof QueueManager.UpdateOpenService) {
                            QueueManager.UpdateOpenService data = (QueueManager.UpdateOpenService) event;
                            updateEnableService(data.getServiceId());
                        } else if (event instanceof DDPManager.MobileDeleteQueueEvent) {
                            DDPManager.MobileDeleteQueueEvent data = (DDPManager.MobileDeleteQueueEvent) event;
                            if (!TextUtils.isEmpty(currentQueueManaging)
                                    && manageQueueDialogFragmentViewController != null
                                    && manageQueueDialogFragmentViewController.isVisible()
                                    && data.getQueueId().equalsIgnoreCase(currentQueueManaging)) {
                                manageQueueDialogFragmentViewController.close();
                            }
                        }
                    }
                }));
        currentAvailableQueue = RxSharedPreferences.create(TabletConfig.pref().getSharedPreferences()).getInteger("queueCurrentQueueReserve");
        subscriptionCurrentAvailableQueue = new CompositeSubscription();
        subscriptionCurrentAvailableQueue.add(currentAvailableQueue.asObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Integer>() {
                    @Override
                    public void call(Integer s) {
                        textviewQueueAvailableReserve.setTextColor(ContextCompat.getColor(getApplicationContext(), s == TabletConfig.pref().getQueueMaxAvailableReserve() ? R.color.red_ci : R.color.grey_light));
                        textviewQueueAvailableReserve.setText(String.format("%d/%d", s, TabletConfig.pref().getQueueMaxAvailableReserve()));
                    }
                }));


        checkConnection();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (!subscriptions.isUnsubscribed())
            subscriptions.unsubscribe();
        if (!subscriptionCurrentAvailableQueue.isUnsubscribed())
            subscriptionCurrentAvailableQueue.unsubscribe();
    }

    @Override
    protected void onDestroy() {
        subscriptions.unsubscribe();
        subscriptionCurrentAvailableQueue.unsubscribe();
        DDPManager.getInstance().close();
        super.onDestroy();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        ButterKnife.bind(this);
        initData();
        initTabMenu();
        initView();

        QueueManager.getInstance().init(MainActivity.this, new QueueManager.LoadQueueListener() {
            @Override
            public void onLoadSuccess() {
                for (int i = 0; i < listTab.size(); i++) {
                    listTab.get(i).setNoQueue(QueueManager.getInstance().getListQueueByQueueState(QueueManager.getInstance().getState(i)).size());
                }
                onClickTab(listTab.get(0));
                if (DDPManager.getMeteor() == null || !DDPManager.getInstance().isConnect())
                    DDPManager.getInstance().init(MainActivity.this, new DDPManager.ConnectServerListener() {
                        @Override
                        public void onSuccess() {

                            observableMeteorStatus = DDPManager.getInstance().subscribeStatus();
                            observableMeteorStatus.subscribe(new Action1<Boolean>() {
                                @Override
                                public void call(Boolean aBoolean) {
                                    setConnectionStatus(aBoolean);

                                }
                            });
//                            if (!isInitHardware) {
//                                isInitHardware = true;
//                                connectPrinter();
//                            }
                            beforeCheckQueue();
                        }

                        @Override
                        public void onFail() {
//                            if (!isInitHardware) {
//                                isInitHardware = true;
//                                connectPrinter();
//                            }
                            beforeCheckQueue();
                            setConnectionStatus(false);
                        }
                    });


            }
        });
        btnReserveQueue.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (TabletConfig.pref().getQueueCurrentQueueReserve() < TabletConfig.pref().getQueueMaxAvailableReserve())
                    onClickReserveQueue();
                else {
                    Bundle bundle = new Bundle();
                    bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "Main");
                    bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Queue limit");
                    App.mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);

                    DialogHelper.openLimitQueueDialog(MainActivity.this);
                }
            }
        });
    }

    private void initData() {
        mDatabase = FirebaseDatabase.getInstance().getReference();
        DatabaseReference maxQ = mDatabase.child("max_queue");
        maxQ.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot != null) {
                    int max = dataSnapshot.getValue(int.class);
                    if (max != TabletConfig.pref().getQueueMaxAvailableReserve()) {
                        TabletConfig.pref().setQueueMaxAvailableReserve(max);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        currentQ = mDatabase.child("users").child(TabletConfig.pref().getUserId()).child("current");
        currentQ.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot != null) {
                    int current = dataSnapshot.getValue(int.class);
                    if (current != TabletConfig.pref().getQueueCurrentQueueReserve()) {
                        TabletConfig.pref().setQueueCurrentQueueReserve(current);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

//    private void connectPrinter() {
//        if (TabletConfig.pref().isEnablePrinter()) {
//            PrinterManager.getInstance().init(MainActivity.this);
//            PrinterManager.getInstance().checkPrint(new PrintManager.CheckPrintListener() {
//                @Override
//                public void onCheckSuccess(String macAddr) {
//                }
//
//                @Override
//                public void onFail() {
//                }
//            });
//        } else {
//            /**
//             * Alert Promotion
//             */
//            DialogHelper.openDialogUsePrinter(MainActivity.this);
//        }
//        beforeCheckQueue();
//    }

    private void beforeCheckQueue() {
        Observable.timer(2000, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Long>() {
                    @Override
                    public void call(Long aLong) {
                        checkResetAllQueueByTime();
                    }
                });
    }


    public void onClickTab(View view) {
        for (TabMenuButton tab : listTab) {
            tab.setSelected(tab.getId() == view.getId());
            if (tab.getId() == view.getId()) {
                if (listQueue == null)
                    listQueue = new ArrayList<>();
                currentView = Integer.valueOf(String.valueOf(view.getTag()));
                updateQueueInCurrentTab(currentView);
            }
        }
    }

    private void updateQueueInCurrentTab(int tab) {
        listQueue.clear();
        switch (tab) {
            case 0:
                listQueue.addAll(QueueManager.getInstance().getListQueue());
                break;
            case 1:
                listQueue.addAll(QueueManager.getInstance().getListSkip());
                break;
            case 2:
                listQueue.addAll(QueueManager.getInstance().getListGetin());
                break;
        }
        queueAdapter.notifyDataSetChanged();
    }

    private void initTabMenu() {
        listTabName = getResources().getStringArray(R.array.array_tab_name);
        for (int i = 0; i < listTab.size(); i++) {
            listTab.get(i).setTabName(listTabName[i]);
        }
    }

    private void initView() {
        brachName.setText(TabletConfig.pref().getBranchName());
        listQueue = new ArrayList<>();
        queueAdapter = new QueueAdapter(getApplicationContext(), listQueue);
        queueAdapter.setListener(this);
        gridLayoutManager = new GridAutofitLayoutManager(getApplicationContext(), (int) getResources().getDimension(R.dimen.queue_width_height));
        recycleviewQueue.setHasFixedSize(true);
        recycleviewQueue.setLayoutManager(gridLayoutManager);
        recycleviewQueue.setAdapter(queueAdapter);
    }

    private void onClickReserveQueue() {
        ReserveQueueDialogFragmentViewController reserveQueueDialogFragmentViewController = ReserveQueueDialogFragmentViewController.newInstance();
        reserveQueueDialogFragmentViewController.setListener(new ReserveQueueDialogFragmentViewController.ReserveListener() {
            @Override
            public void onClickReserveQueue(String name, String telephone, int noSeat, String serviceId, String serviceName) {
                /**
                 * Add Queue
                 */
                RealmQueue queue = new RealmQueue();
                queue.setQueueId(EncodeHelper.getmInstance().getKey(DateHelper.getMilliseconds()));
                queue.setOrderNumber(String.valueOf(QueueManager.getInstance().getSumQueueInDB()));
                queue.setRefCode(EncodeHelper.getmInstance().getRef(queue.getOrderNumber(), DateHelper.getCurrentDateTime().getDayOfMonth(), TabletConfig.pref().getBranchIndex()));
                RealmUser user = new RealmUser();
                user.setId(TextUtils.isEmpty(name) ? null : "1");
                user.setName(TextUtils.isEmpty(name) ? "Guest" : name);
                user.setTelephone(TextUtils.isEmpty(telephone) ? "" : telephone);
                user.setLocal("en");

                queue.setUser(user);
                queue.setReset(false);
                queue.setQueueGroup("A");
                queue.setQueueIndex(String.valueOf(QueueManager.getInstance().getQueueIndex()));
                queue.setIsActive(true);
                queue.setBranchId(TabletConfig.pref().getBranchId());
                queue.setQueueType(QueueType.PAPER.getValue());
                queue.setQueueAction(QueueAction.ACCEPT.getValue());
                queue.setQueueState(QueueStatus.QUEUE_STATUS_QUEUE.getValue());
                queue.setNumSeat(noSeat);
                queue.setTimeReserve(DateHelper.getCurrent());
                queue.setQueueRemain(QueueManager.getInstance().getListQueue().size() + 1);
                queue.setRemainStart(queue.getQueueRemain());
                if (!TextUtils.isEmpty(serviceId)) {
                    queue.setServiceId(serviceId);
                    queue.setServiceName(serviceName);
                }
                QueueManager.getInstance().addQueue("A", queue, new QueueManager.AddQueueListener() {
                    @Override
                    public void onAddSuccess() {
                        RxBus.getInstance().send(new QueueManager.UpdateQueueChangeEvent());

                        TabletConfig.pref().setQueueCurrentQueueReserve(TabletConfig.pref().getQueueCurrentQueueReserve() + 1);
                        currentQ.setValue(TabletConfig.pref().getQueueCurrentQueueReserve());
                        if (TabletConfig.pref().getQueueCurrentQueueReserve() == TabletConfig.pref().getQueueMaxAvailableReserve()) {
                            if (DDPManager.getInstance().isConnect()) {
                                DDPManager.getInstance().setAllowMobileReserve(false, new DDPManager.AllowMobileReserveListener() {
                                    @Override
                                    public void onSuccess() {
                                        DialogHelper.openLimitQueueDialog(MainActivity.this);
                                    }

                                    @Override
                                    public void onFail() {

                                    }
                                });
                            }
                        }
                    }

                    @Override
                    public void onFail(String message) {
                        Log.e("Queue", message);
                    }
                });
                if (TabletConfig.pref().isEnablePrinter() && TabletConfig.pref().isAutoPrint() && PrinterManager.getInstance() != null) {
                    PrinterManager.getInstance().printQueue(3, queue.getTimeReserve(), String.format("%s%s", queue.getQueueGroup(), queue.getQueueIndex()), String.format("%d Seats", queue.getNumSeat()), Utils.getCode(queue), queue.getQueueRemain(), queue.getServiceName(), queue.getRefCode());
                }
            }
        });
        reserveQueueDialogFragmentViewController.show(getSupportFragmentManager(), "reserve");
    }

    @Override
    public void onClickQueue(final int position) {
        if (listQueue.get(position).isCancel()) {
            DialogHelper.openAlertDialogTwoWay(MainActivity.this
                    , getString(R.string.text_delete_queue)
                    , String.format(getString(R.string.text_confirm_delete_queue)
                            , listQueue.get(position).getQueueGroup()
                            , listQueue.get(position).getQueueIndex())
                    , new DialogHelper.DialogCallback() {
                        @Override
                        public void onClickOK() {
                            QueueManager.getInstance().deleteInactiveQueue(listQueue.get(position));
                        }

                        @Override
                        public void onClickCancel() {

                        }
                    });
        } else {
            onOpenQueueDetail(listQueue.get(position), currentView == 0 ? QueueState.QUEUE : currentView == 1 ? QueueState.HOLD : QueueState.GET_IN);
        }

    }

    private void onOpenQueueDetail(RealmQueue queue, QueueState type) {
        currentQueueManaging = queue.getQueueId();
        manageQueueDialogFragmentViewController = ManageQueueDialogFragmentViewController.newInstance(queue, type);
        manageQueueDialogFragmentViewController.show(getSupportFragmentManager(), "detail");
    }

    private void checkConnection() {
        if (observableNetwork != null)
            observableNetwork.unsubscribe();
        observableNetwork = new CompositeSubscription();
        observableNetwork.add(
                ReactiveNetwork.observeInternetConnectivity(5000, "www.google.com", 80, 2000)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Action1<Boolean>() {
                            @Override
                            public void call(Boolean isConnectedToInternet) {
                                isOnline = isConnectedToInternet;
                                Log.d("Check", ">>>>>>>>>>>>Internet " + (isConnectedToInternet ? "Connected" : " Not Connect") + " <<<<<<<<<<<<");
                                setConnectionStatus(isConnectedToInternet);
                            }
                        }));
    }

    private void setConnectionStatus(boolean isStatus) {
        textviewServerStatus.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), isStatus ? R.color.green : R.color.red_ci));
    }

    private void updateEnableService(final String serviceId) {
        ServicesService service = ServiceGenerator.createService(ServicesService.class);
        Call<ServiceResponse> call = service.updateService(new ServiceParam(TabletConfig.pref().getBranchId(), Arrays.asList(serviceId)));
        call.enqueue(new Callback<ServiceResponse>() {
            @Override
            public void onResponse(Call<ServiceResponse> call, final Response<ServiceResponse> response) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (response.code() == 200) {
                            Toast.makeText(getApplicationContext(), "Update Service Enable Success", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getApplicationContext(), "Update Service Enable Fail", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }

            @Override
            public void onFailure(Call<ServiceResponse> call, Throwable t) {

            }
        });
    }

    private void checkResetAllQueueByTime() {
        viewOverlay.setVisibility(View.GONE);
        if (ConfigApplication.prefs().getLastestSentReport() != -1L) {
            DateTime lastUpdate = new DateTime(ConfigApplication.prefs().getLastestSentReport());
            DateTime currentTime = DateHelper.getCurrentDateTime();
            DateTime timeReset = new DateTime().withZone(DateTimeZone.forID("Asia/Bangkok")).withHourOfDay(4).withMinuteOfHour(0).withSecondOfMinute(0);
            if (currentTime.isAfter(timeReset) && lastUpdate.isBefore(timeReset)) {
                /**
                 *
                 * Reset All Queue
                 */
                DialogHelper.showLoadingDialog(MainActivity.this, getString(R.string.text_alert_connecting_to_server), getString(R.string.text_alert_connecting_to_server));
                QueueManager.getInstance().resetQueueAutoTime(new QueueManager.ResetQueueListener() {
                    @Override
                    public void onSuccess() {
                        DialogHelper.hideLoadingDialog();
                    }

                    @Override
                    public void onError() {
                        DialogHelper.hideLoadingDialog();
                    }
                });
            }
        }
    }

    private boolean exit = false;

    @Override
    public void onBackPressed() {
        if (exit) {
            super.onBackPressed();

        } else {
            Toast.makeText(this, "Press back again to exit.", Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 2 * 1000);
        }
    }
}
