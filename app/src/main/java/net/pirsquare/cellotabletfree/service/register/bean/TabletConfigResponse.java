package net.pirsquare.cellotabletfree.service.register.bean;

import com.google.gson.annotations.SerializedName;

import net.pirsquare.cellotabletfree.model.TabletType;

import java.util.List;

/**
 * Created by kung on 6/16/16.
 */
public class TabletConfigResponse {


    private String status;
    private String message;
    private ResultsBean results;

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ResultsBean getResults() {
        return results;
    }

    public void setResults(ResultsBean results) {
        this.results = results;
    }

    public static class ResultsBean {
        private BranchBean branch;

        private ConfigsBean configs;

        private DeviceSetting deviceSettings;

        public BranchBean getBranch() {
            return branch;
        }

        public ConfigsBean getConfigs() {
            return configs;
        }


        public static class BranchBean {
            private String _id;
            private String name;
            private String brandId;
            private int index;

            private List<ServiceModel> services;

            public int getIndex() {
                return index;
            }

            public String getId() {
                return _id;
            }

            public String getName() {
                return name;
            }

            public String getBrandId() {
                return brandId;
            }

            public List<ServiceModel> getServices() {
                return services;
            }

            public static class ServiceModel {
                private String id;
                @SerializedName("icon_url")
                private String iconUrl;
                @SerializedName("is_allow_capacity")
                private String isAllowCapacity;
                private String name;
                @SerializedName("capacity_unit")
                private String capacityUnit;
                private int sort;

                public String getId() {
                    return id;
                }


                public String getIconUrl() {
                    return iconUrl;
                }

                public String getIsAllowCapacity() {
                    return isAllowCapacity;
                }

                public String getName() {
                    return name;
                }

                public String getCapacityUnit() {
                    return capacityUnit;
                }


                public int getSort() {
                    return sort;
                }

                @Override
                public boolean equals(Object o) {
                    if (this == o) return true;
                    if (!(o instanceof ServiceModel)) return false;

                    ServiceModel that = (ServiceModel) o;

                    if (id != null ? !id.equals(that.id) : that.id != null) return false;
                    if (iconUrl != null ? !iconUrl.equals(that.iconUrl) : that.iconUrl != null)
                        return false;
                    if (isAllowCapacity != null ? !isAllowCapacity.equals(that.isAllowCapacity) : that.isAllowCapacity != null)
                        return false;
                    if (name != null ? !name.equals(that.name) : that.name != null) return false;
                    return capacityUnit != null ? capacityUnit.equals(that.capacityUnit) : that.capacityUnit == null;

                }
            }
        }

        public static class ConfigsBean {

            private AwsS3Bean awsS3;
            private String logo;
            private PrinterBean printers;
            boolean isHoldable;
            private int holdExpireSecond;
            private TabletType clientType;
            private boolean allowGroupManage;
            private DefaultSettingsBean defaultSettings;


            public AwsS3Bean getAwsS3() {
                return awsS3;
            }


            public String getLogo() {
                return logo;
            }

            public PrinterBean getPrinter() {
                return printers;
            }

            public int getHoldExpire() {
                return holdExpireSecond;
            }

            public boolean isHoldable() {
                return isHoldable;
            }

            public TabletType getClientType() {
                return clientType;
            }

            public boolean isAllowGroupManage() {
                return allowGroupManage;
            }

            public DefaultSettingsBean getDefaultSettings() {
                return defaultSettings;
            }


            public static class AwsS3Bean {
                private String S3_BUCKET_NAME;
                private String S3_KEY_DIRECTORY;

                public String getS3BucketName() {
                    return S3_BUCKET_NAME;
                }


                public String getS3KeyDirectory() {
                    return S3_KEY_DIRECTORY;
                }

            }

            public static class PrinterBean {
                private String logoTop;
                private String logoBottom;

                public String getLogoTop() {
                    return logoTop;
                }

                public String getLogoBottom() {
                    return logoBottom;
                }

            }

            public static class DefaultSettingsBean {

                private boolean allowReserve;
                private List<GroupManagementBean> groupManagement;

                public List<GroupManagementBean> getGroupManagement() {
                    return groupManagement;
                }

                public boolean isAllowReserve() {
                    return allowReserve;
                }

                public static class GroupManagementBean {
                    private String serviceId;
                    private int maxCapacity;
                    private int minCapacity;
                    private boolean allowBooking;

                    public String getServiceId() {
                        return serviceId;
                    }

                    public int getMaxCapacity() {
                        return maxCapacity;
                    }

                    public int getMinCapacity() {
                        return minCapacity;
                    }

                    public boolean isAllowBooking() {
                        return allowBooking;
                    }
                }
            }
        }
    }
}
