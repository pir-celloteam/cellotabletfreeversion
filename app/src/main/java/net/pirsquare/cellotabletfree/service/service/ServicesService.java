package net.pirsquare.cellotabletfree.service.service;

import net.pirsquare.cellotabletfree.service.service.bean.ServiceParam;
import net.pirsquare.cellotabletfree.service.service.bean.ServiceResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.PUT;

/**
 * Created by kung on 8/16/16.
 */
public interface ServicesService {
    @Headers("Content-Type:application/json")
    @PUT("services")
    Call<ServiceResponse> updateService(@Body ServiceParam param);
}
