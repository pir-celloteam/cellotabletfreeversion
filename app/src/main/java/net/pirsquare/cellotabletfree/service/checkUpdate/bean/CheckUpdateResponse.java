package net.pirsquare.cellotabletfree.service.checkUpdate.bean;

/**
 * Created by kung on 7/20/16.
 */
public class CheckUpdateResponse {


    /**
     * status : ok
     * data : {"forceUpgrade":true,"refs":"restaurant_arm_2.0.0.apk"}
     */

    private String status;
    /**
     * forceUpgrade : true
     * refs : restaurant_arm_2.0.0.apk
     */

    private DataBean data;

    public String getStatus() {
        return status;
    }

    public DataBean getData() {
        return data;
    }

    public static class DataBean {
        private boolean forceUpgrade;
        private String refs;
        private String forceVersion;

        public boolean isForceUpgrade() {
            return forceUpgrade;
        }

        public String getRefs() {
            return refs;
        }

        public String getVersion() {
            return forceVersion;
        }
    }
}
