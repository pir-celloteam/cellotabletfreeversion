package net.pirsquare.cellotabletfree.service.service.bean;

/**
 * Created by kung on 8/16/16.
 */
public class ServiceResponse {

    /**
     * status : ok
     * message : Success
     */

    private String status;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
