package net.pirsquare.cellotabletfree.service.register;

import net.pirsquare.cellotabletfree.service.register.bean.RegisParam;
import net.pirsquare.cellotabletfree.service.register.bean.TabletConfigResponse;
import net.pirsquare.cellotabletfree.service.register.bean.VerifyResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by kung on 6/13/16.
 */
public interface RegisterService {

    @Headers("Content-Type:application/json")
    @POST("accounts/verify")
    Call<VerifyResponse> checkVerify(@Body RegisParam data);

    @Headers("Content-Type:application/json")
    @GET("accounts/configs")
    Call<TabletConfigResponse> getConfig(@Query("_id") String id);

}
