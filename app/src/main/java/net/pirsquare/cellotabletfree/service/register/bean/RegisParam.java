package net.pirsquare.cellotabletfree.service.register.bean;

/**
 * Created by kung on 6/13/16.
 */
public class RegisParam {
    private String username;
    private String password;
    private DeviceSetting deviceSettings;



    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public void setTvMacAddress(String tvMacAddress) {
        if (deviceSettings == null)
            deviceSettings = new DeviceSetting();
        this.deviceSettings.tvMacAddress = tvMacAddress;
    }

    public void setPrinterMacAddress(String printerMacAddress) {
        if (deviceSettings == null)
            deviceSettings = new DeviceSetting();
        this.deviceSettings.printerMacAddress = printerMacAddress;
    }

    public void setTabletMacAddress(String tabletMacAddress) {
        if (deviceSettings == null)
            deviceSettings = new DeviceSetting();
        this.deviceSettings.tabletMacAddress = tabletMacAddress;
    }

    public void setDeviceDetail(String appVersion, String osVersion) {
        if (deviceSettings == null)
            deviceSettings = new DeviceSetting();
        this.deviceSettings.appVersion = appVersion;
        this.deviceSettings.deviceType = "android";
        this.deviceSettings.osVersion = osVersion;
    }

}
