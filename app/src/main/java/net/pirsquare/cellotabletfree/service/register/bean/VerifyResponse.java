package net.pirsquare.cellotabletfree.service.register.bean;

/**
 * Created by kung on 6/13/16.
 */
public class VerifyResponse {
    private String status;
    private String message;

    private ResultsBean results;


    public ResultsBean getResults() {
        return results;
    }

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public static class ResultsBean {
        private String _id;

        public String getId() {
            return _id;
        }

    }
}
