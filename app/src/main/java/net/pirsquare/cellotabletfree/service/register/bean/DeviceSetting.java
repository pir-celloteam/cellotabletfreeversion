package net.pirsquare.cellotabletfree.service.register.bean;

public class DeviceSetting {
    String tvMacAddress;
    String printerMacAddress;
    String tabletMacAddress;

    String appVersion;
    String deviceType;
    String osVersion;
}