package net.pirsquare.cellotabletfree.service.checkUpdate;

import net.pirsquare.cellotabletfree.service.checkUpdate.bean.CheckUpdateResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by kung on 7/20/16.
 */
public interface CheckUpdateService {
    @GET("check-updates")
    Call<CheckUpdateResponse> checkUpdateVersion(@Query("software") String softType, @Query("ver") String version);
}