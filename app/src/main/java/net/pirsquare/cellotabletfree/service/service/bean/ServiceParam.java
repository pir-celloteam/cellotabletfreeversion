package net.pirsquare.cellotabletfree.service.service.bean;

import java.util.List;

/**
 * Created by kung on 8/16/16.
 */
public class ServiceParam {

    private String branchId;
    private List<String> services;

    public ServiceParam(String branchId, List<String> services) {
        this.branchId = branchId;
        this.services = services;
    }

}
