package net.pirsquare.cellotabletfree.login;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.github.kittinunf.fuel.Fuel;
import com.github.kittinunf.fuel.core.FuelError;
import com.github.kittinunf.fuel.core.Handler;
import com.github.kittinunf.fuel.core.Request;
import com.github.kittinunf.fuel.core.Response;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.orhanobut.logger.Logger;

import net.pirsquare.cellotabletfree.R;
import net.pirsquare.cellotabletfree.application.App;
import net.pirsquare.cellotabletfree.manager.DialogHelper;
import net.pirsquare.cellotabletfree.util.Utils;

import org.jetbrains.annotations.NotNull;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class RegisterActivity extends AppCompatActivity {

    @BindView(R.id.edittext_serial)
    EditText etName;
    @BindView(R.id.edittext_shopname)
    EditText etShop;
    @BindView(R.id.edittext_phone)
    EditText etPhone;
    @BindView(R.id.edittext_email)
    EditText etEmail;
    @BindView(R.id.btn_res)
    Button btnRegis;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);

    }

    @OnClick(R.id.btn_res)
    void registerUser() {
        if (checkEmptyData()) {
            if (Utils.isValidEmail(etEmail.getText().toString().trim())) {
                DialogHelper.showProgressDialog(RegisterActivity.this, null, "registering...");

                Bundle bundle = new Bundle();
                bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "Register");
                bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "click register");
                App.mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);

                Observable.timer(2, TimeUnit.SECONDS)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(new Action1<Long>() {
                            @Override
                            public void call(Long aLong) {
                                sendAPI();
                            }
                        });
            } else {
                Toast.makeText(RegisterActivity.this, getString(R.string.mail_invalid), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(RegisterActivity.this, getString(R.string.fill_empty), Toast.LENGTH_SHORT).show();
        }
    }

    private boolean checkEmptyData() {
        return !(TextUtils.isEmpty(etName.getText().toString().trim())) && !(TextUtils.isEmpty(etShop.getText().toString().trim()))
                && !(TextUtils.isEmpty(etPhone.getText().toString().trim())) && !(TextUtils.isEmpty(etEmail.getText().toString().trim()));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
    }

    private void sendAPI() {
        String url = "http://www.pirsquare.net/project/cello/submitBranchInfo.php?name=" + etName.getText().toString().trim() +
                "&shop_name=" + etShop.getText().toString().trim() + "&phone=" + etPhone.getText().toString().trim() + "&email=" + etEmail.getText().toString().trim();

        Fuel.get(url).response(new Handler<byte[]>() {
            @Override
            public void success(@NotNull Request request, @NotNull Response response, byte[] bytes) {
                DialogHelper.hideProgressDialog();
                Logger.d("send api success");
                DialogHelper.openAlertDialogForceCancel(RegisterActivity.this, getString(R.string.successful), getString(R.string.wait_callback), new DialogHelper.DialogCallback() {
                    @Override
                    public void onClickOK() {
                        onBackPressed();
                    }

                    @Override
                    public void onClickCancel() {

                    }
                });

            }

            @Override
            public void failure(@NotNull Request request, @NotNull Response response, @NotNull FuelError fuelError) {
                DialogHelper.hideProgressDialog();
                Logger.d("send api failure");
                Logger.d(response.toString());
                DialogHelper.openAlertDialogForceCancel(RegisterActivity.this, getString(R.string.fail), getString(R.string.fail_try_again), new DialogHelper.DialogCallback() {
                    @Override
                    public void onClickOK() {

                    }

                    @Override
                    public void onClickCancel() {

                    }
                });

            }
        });
    }
}
