package net.pirsquare.cellotabletfree.login;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.pwittchen.reactivenetwork.library.BuildConfig;
import com.github.pwittchen.reactivenetwork.library.ReactiveNetwork;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.orhanobut.logger.Logger;

import net.pirsquare.cellotabletfree.R;
import net.pirsquare.cellotabletfree.application.App;
import net.pirsquare.cellotabletfree.main.MainActivity;
import net.pirsquare.cellotabletfree.manager.DialogHelper;
import net.pirsquare.cellotabletfree.manager.RegisterHelper;
import net.pirsquare.cellotabletfree.manager.TabletConfig;
import net.pirsquare.cellotabletfree.model.ReservedQueue;
import net.pirsquare.cellotabletfree.model.Service;
import net.pirsquare.cellotabletfree.service.ServiceGenerator;
import net.pirsquare.cellotabletfree.service.register.RegisterService;
import net.pirsquare.cellotabletfree.service.register.bean.TabletConfigResponse;
import net.pirsquare.cellotabletfree.service.register.bean.TabletConfigResponse.ResultsBean.BranchBean.ServiceModel;
import net.pirsquare.cellotabletfree.service.register.bean.TabletConfigResponse.ResultsBean.ConfigsBean.DefaultSettingsBean.GroupManagementBean;
import net.pirsquare.cellotabletfree.service.register.bean.VerifyResponse;
import net.pirsquare.cellotabletfree.util.GsonHelper;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import java8.util.function.Predicate;
import java8.util.stream.StreamSupport;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static net.pirsquare.cellotabletfree.manager.DialogHelper.hideProgressDialog;

@RuntimePermissions
public class LoginActivity extends AppCompatActivity {
//    public FirebaseRemoteConfig mFirebaseRemoteConfig;

    @BindView(R.id.view_loading)
    FrameLayout viewLoading;
    @BindView(R.id.edittext_serial)
    EditText edittextId;
    @BindView(R.id.edittext_password)
    EditText edittextPassword;
    //    @BindView(R.id.edittext_printer_ip_address)
//    EditText edittextPrinterIpaddress;
//    @BindView(R.id.textview_tv_mac)
//    TextView textviewTvMac;
//    @BindView(R.id.textview_printer_mac)
//    TextView textviewPrinterMac;
    @BindView(R.id.textview_register_for_branch)
    TextView textviewRegisterForBranch;
    @BindView(R.id.btn_joy)
    Button btnJoy;
    @BindView(R.id.btn_register)
    Button btnRegister;
    @BindView(R.id.view_logo)
    RelativeLayout viewLogo;
    private boolean tvAvailable = false;
    private boolean printerAvailable = false;
    private String printerMac;
    private String tabletMac;
    private CompositeSubscription observableNetwork;
    private boolean isOnline = false;
    private boolean isNewUser = false;

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private DatabaseReference myRef;
    private FirebaseUser user;

    @Override
    protected void onStart() {
        super.onStart();
        //        setupRemoteConfig();
        mAuth.addAuthStateListener(mAuthListener);
        checkAvailableInternet();

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    @Override
    protected void onDestroy() {
        if (observableNetwork != null)
            observableNetwork.unsubscribe();
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (observableNetwork != null)
            observableNetwork.unsubscribe();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppTheme);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        RegisterHelper.getInstance();

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        myRef = database.getReference("users");

        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Logger.d("onAuthStateChanged:signed_in:");
                    TabletConfig.pref().setUserId(user.getUid());

                } else {
                    // User is signed out
                    Logger.d("onAuthStateChanged:signed_out");
                }
            }
        };

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://q-happy.com/"));
//                startActivity(browserIntent);
                Bundle bundle = new Bundle();
                bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "Login");
                bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "click register");
                App.mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
            }
        });

        btnJoy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkConnection();
            }
        });

        checkLogin();
    }


    private void checkLogin() {
        Observable.timer(3000, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(aLong -> {
                    if (TabletConfig.pref().isLogin()) {
                        btnRegister.setClickable(false);
                        btnJoy.setClickable(false);
                        LoginActivityPermissionsDispatcher.requestPermissionWithCheck(this);
                    } else {
                        WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
                        WifiInfo wInfo = wifiManager.getConnectionInfo();
                        tabletMac = wInfo.getMacAddress();
                        viewLogo.setVisibility(View.GONE);
                    }
                });
    }

    @NeedsPermission({Manifest.permission.BLUETOOTH, Manifest.permission.BLUETOOTH_ADMIN, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE})
    public void requestPermission() {
        if (isOnline) {
            updateTabletConfigData();
            signInFirebase(TabletConfig.pref().getUser(), TabletConfig.pref().getSerialNumber());
        } else {
            viewLogo.setVisibility(View.GONE);
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
            finish();
            System.gc();
        }
    }

    @NeedsPermission({Manifest.permission.BLUETOOTH, Manifest.permission.BLUETOOTH_ADMIN, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE})
    public void checkConnection() {
        if (!TextUtils.isEmpty(edittextId.getText().toString().trim()) && !TextUtils.isEmpty(edittextPassword.getText().toString().trim())) {
            RegisterHelper.getInstance().setUsername(edittextId.getText().toString().trim());
            RegisterHelper.getInstance().setSerialKey(edittextPassword.getText().toString().trim());
            RegisterHelper.getInstance().setAppDetail(BuildConfig.VERSION_NAME, String.valueOf(Build.VERSION.SDK_INT));
            RegisterHelper.getInstance().setTabletMacAddress(tabletMac);
            checkRegister();

        } else {
            Toast.makeText(LoginActivity.this, getString(R.string.fill_empty), Toast.LENGTH_LONG).show();
        }
    }

    private void checkRegister() {
        viewLoading.setVisibility(View.VISIBLE);
        RegisterService service = ServiceGenerator.createService(RegisterService.class);
        Call<VerifyResponse> call = service.checkVerify(RegisterHelper.getInstance().getParam());
        call.enqueue(new Callback<VerifyResponse>() {
            @Override
            public void onResponse(Call<VerifyResponse> call, Response<VerifyResponse> response) {
                VerifyResponse model = response.body();
                runOnUiThread(() -> {
                    viewLoading.setVisibility(View.GONE);
                    if (response.code() == 200) {
                        if (model != null && model.getStatus().equalsIgnoreCase("ok")) {
                            /**
                             * Conplete Verify
                             */
                            TabletConfig.pref().setIdTablet(model.getResults().getId());
                            getConfigTablet();
                        } else if (model != null && model.getStatus().equalsIgnoreCase("error")) {
                            /**
                             * Verify Error
                             */
                            DialogHelper.openAlertDialog(LoginActivity.this, getString(R.string.text_alert_register_tablet_device_not_finish), model.getMessage(), null);
                        }
                    } else {
                        DialogHelper.openAlertDialog(LoginActivity.this, getString(R.string.text_alert_register_tablet_device_not_finish), getString(R.string.text_alert_system_error_please_try_agin), null);
                    }
                });


            }

            @Override
            public void onFailure(Call<VerifyResponse> call, Throwable t) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        viewLoading.setVisibility(View.GONE);
                        DialogHelper.openAlertDialog(LoginActivity.this, getString(R.string.text_alert_register_tablet_device_on_fail), t.getMessage(), null);
                    }
                });
            }
        });
    }

    private void getConfigTablet() {
        viewLoading.setVisibility(View.VISIBLE);
        RegisterService service = ServiceGenerator.createService(RegisterService.class);
        Call<TabletConfigResponse> call = service.getConfig(TabletConfig.pref().getIdTablet());
        call.enqueue(new Callback<TabletConfigResponse>() {
            @Override
            public void onResponse(Call<TabletConfigResponse> call, Response<TabletConfigResponse> response) {
                TabletConfigResponse model = response.body();
                runOnUiThread(() -> {
                    if (response.code() == 200) {
                        if (model != null && model.getStatus().equalsIgnoreCase("ok")) {

                            btnJoy.setEnabled(false);
                            btnRegister.setEnabled(false);
                            textviewRegisterForBranch.setText(getString(R.string.text_branch_register) + model.getResults().getBranch().getName() + getString(R.string.text_branch_success_register));
                            TabletConfig.pref().setAppType(model.getResults().getConfigs().getClientType().getValue());
                            /**
                             * Manage Default Service
                             */
                            if (model.getResults().getBranch().getServices() != null && model.getResults().getConfigs().getDefaultSettings().getGroupManagement() != null) {
                                ArrayList<Service> listService = new ArrayList<Service>();
                                for (GroupManagementBean data : model.getResults().getConfigs().getDefaultSettings().getGroupManagement()) {
                                    ServiceModel service = StreamSupport.stream(model.getResults().getBranch().getServices()).filter(new Predicate<ServiceModel>() {
                                        @Override
                                        public boolean test(ServiceModel serviceModel) {
                                            return serviceModel.getId().equalsIgnoreCase(data.getServiceId());
                                        }
                                    }).findFirst().get();
                                    if (service != null) {
                                        Log.e("Service", "updateTabletConfigData CanBooking " + service.getName() + " : " + (data.isAllowBooking() ? "Yes" : "No"));
                                        if (service.getIsAllowCapacity().equalsIgnoreCase("yes")) {
                                            listService.add(new Service(service.getId(), service.getName(), service.getIsAllowCapacity().equalsIgnoreCase("yes"), service.getSort(), data.getMinCapacity(), data.getMaxCapacity(), data.isAllowBooking()));
                                        } else {
                                            listService.add(new Service(service.getId(), service.getName(), service.getIsAllowCapacity().equalsIgnoreCase("yes"), service.getSort(), data.isAllowBooking()));
                                        }
                                    }
                                }
                                TabletConfig.pref().setServiceData(GsonHelper.getGson().toJson(listService));
                            }
                            TabletConfig.pref().setBranchIndex(String.valueOf(model.getResults().getBranch().getIndex()));
                            TabletConfig.pref().setIsLogin(true);
                            TabletConfig.pref().setIsAllowGroupManagement(model.getResults().getConfigs().isAllowGroupManage());
                            TabletConfig.pref().setBranchId(model.getResults().getBranch().getId());
                            TabletConfig.pref().setLogo(model.getResults().getConfigs().getLogo());
                            TabletConfig.pref().setBranchName(model.getResults().getBranch().getName());
                            TabletConfig.pref().setS3BucketName(model.getResults().getConfigs().getAwsS3().getS3BucketName());
                            TabletConfig.pref().setS3KeyDirectory(model.getResults().getConfigs().getAwsS3().getS3KeyDirectory());
                            TabletConfig.pref().setIsOpenMobileReserve(model.getResults().getConfigs().getDefaultSettings().isAllowReserve());
                            TabletConfig.pref().setIsEnablePrinter(false);

                            TabletConfig.pref().setSerialNumber(RegisterHelper.getInstance().getSerialKey());
                            TabletConfig.pref().setTabletMacAddress(RegisterHelper.getInstance().getTabletMacAddress());
                            TabletConfig.pref().setUser(RegisterHelper.getInstance().getUsername());

                            signUpFirebase(edittextId.getText().toString().trim(), edittextPassword.getText().toString().trim());

                        } else if (model != null && model.getStatus().equalsIgnoreCase("error")) {
                            DialogHelper.openAlertDialog(LoginActivity.this, getString(R.string.text_alert_load_device_config), model.getMessage(), null);
                        }

                    } else if (response.code() == 404) {
                        /**
                         * Cannot use for this id
                         */
                        if (model != null && model.getStatus().equalsIgnoreCase("error")) {
                            DialogHelper.openAlertDialog(LoginActivity.this, getString(R.string.text_alert_load_device_config), model.getMessage(), null);
                        } else {
                            DialogHelper.openAlertDialog(LoginActivity.this, getString(R.string.text_alert_load_device_config), getString(R.string.text_alert_system_error_please_try_agin), null);
                        }

                    } else {
                        DialogHelper.openAlertDialog(LoginActivity.this, getString(R.string.text_alert_load_device_config), getString(R.string.text_alert_system_error_please_try_agin), null);
                    }
                });

            }

            @Override
            public void onFailure(Call<TabletConfigResponse> call, Throwable t) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        viewLoading.setVisibility(View.GONE);
                        DialogHelper.openAlertDialog(LoginActivity.this, getString(R.string.text_alert_load_device_config), t.getMessage(), null);
                    }
                });
            }
        });
    }

    private void updateTabletConfigData() {
        Log.e("Branch ID", "Branch Id " + TabletConfig.pref().getBranchId());
        viewLoading.setVisibility(View.VISIBLE);
        RegisterService service = ServiceGenerator.createService(RegisterService.class);
        Call<TabletConfigResponse> call = service.getConfig(TabletConfig.pref().getIdTablet());
        call.enqueue(new Callback<TabletConfigResponse>() {
            @Override
            public void onResponse(Call<TabletConfigResponse> call, Response<TabletConfigResponse> response) {
                TabletConfigResponse model = response.body();
                runOnUiThread(() -> {
                    viewLoading.setVisibility(View.GONE);
                    if (response.code() == 200) {
                        if (model != null && model.getStatus().equalsIgnoreCase("ok")) {
                            TabletConfig.pref().setAppType(model.getResults().getConfigs().getClientType().getValue());
                            TabletConfig.pref().setIsLogin(true);
                            TabletConfig.pref().setIsAllowGroupManagement(model.getResults().getConfigs().isAllowGroupManage());

                            /**
                             * Manage Default Service
                             */
                            if (model.getResults().getBranch().getServices() != null && model.getResults().getConfigs().getDefaultSettings().getGroupManagement() != null) {
                                ArrayList<Service> listService = new ArrayList<Service>();
                                for (GroupManagementBean data : model.getResults().getConfigs().getDefaultSettings().getGroupManagement()) {
                                    ServiceModel service = StreamSupport.stream(model.getResults().getBranch().getServices()).filter(new Predicate<ServiceModel>() {
                                        @Override
                                        public boolean test(ServiceModel serviceModel) {
                                            return serviceModel.getId().equalsIgnoreCase(data.getServiceId());
                                        }
                                    }).findFirst().get();
                                    if (service != null) {
                                        Log.e("Service", "updateTabletConfigData " + service.getName() + " : " + (data.isAllowBooking() ? "Yes" : "No"));
                                        if (service.getIsAllowCapacity().equalsIgnoreCase("yes")) {
                                            listService.add(new Service(service.getId(), service.getName(), service.getIsAllowCapacity().equalsIgnoreCase("yes"), service.getSort(), data.getMinCapacity(), data.getMaxCapacity(), data.isAllowBooking()));
                                        } else {
                                            listService.add(new Service(service.getId(), service.getName(), service.getIsAllowCapacity().equalsIgnoreCase("yes"), service.getSort(), data.isAllowBooking()));
                                        }
                                    }
                                }
                                TabletConfig.pref().setServiceData(GsonHelper.getGson().toJson(listService));
                            }
                            TabletConfig.pref().setBranchIndex(String.valueOf(model.getResults().getBranch().getIndex()));
                            TabletConfig.pref().setBranchId(model.getResults().getBranch().getId());
                            TabletConfig.pref().setLogo(model.getResults().getConfigs().getLogo());
                            TabletConfig.pref().setIsAvaiableHold(model.getResults().getConfigs().isHoldable());
                            TabletConfig.pref().setBranchName(model.getResults().getBranch().getName());
                            TabletConfig.pref().setS3BucketName(model.getResults().getConfigs().getAwsS3().getS3BucketName());
                            TabletConfig.pref().setS3KeyDirectory(model.getResults().getConfigs().getAwsS3().getS3KeyDirectory());
                            TabletConfig.pref().setIsOpenMobileReserve(model.getResults().getConfigs().getDefaultSettings().isAllowReserve());

                        } else if (model != null && model.getStatus().equalsIgnoreCase("error")) {
                            DialogHelper.openAlertDialog(LoginActivity.this, getString(R.string.text_alert_load_device_config), model.getMessage(), null);
                        }

                    } else if (response.code() == 404) {
                        /**
                         * Cannot use for this id
                         */
                        if (model != null && model.getStatus().equalsIgnoreCase("error")) {
                            DialogHelper.openAlertDialog(LoginActivity.this, getString(R.string.text_alert_load_device_config), model.getMessage(), new DialogHelper.DialogCallback() {
                                @Override
                                public void onClickOK() {

                                }

                                @Override
                                public void onClickCancel() {

                                }
                            });
                        } else {
                            DialogHelper.openAlertDialog(LoginActivity.this, getString(R.string.text_alert_load_device_config), getString(R.string.text_alert_system_error_please_try_agin), new DialogHelper.DialogCallback() {
                                @Override
                                public void onClickOK() {
                                }

                                @Override
                                public void onClickCancel() {

                                }
                            });
                        }

                    } else {
                        DialogHelper.openAlertDialog(LoginActivity.this, getString(R.string.text_alert_load_device_config), getString(R.string.text_alert_system_error_please_try_agin), new DialogHelper.DialogCallback() {
                            @Override
                            public void onClickOK() {
                            }

                            @Override
                            public void onClickCancel() {

                            }
                        });
                    }
                });

            }

            @Override
            public void onFailure(Call<TabletConfigResponse> call, Throwable t) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), getString(R.string.text_alert_load_device_config), Toast.LENGTH_SHORT).show();
                                startMainActivity();
                            }
                        });
                    }
                });
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        LoginActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void checkAvailableInternet() {
        if (observableNetwork == null)
            observableNetwork = new CompositeSubscription();
        observableNetwork.add(
                ReactiveNetwork.observeInternetConnectivity()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Action1<Boolean>() {
                            @Override
                            public void call(Boolean isConnectedToInternet) {
                                Log.e("SetupActivity", isConnectedToInternet ? "Internet Connected" : "Internet Not Connect");
                                isOnline = isConnectedToInternet;
                            }
                        }));
    }

    private void startMainActivity() {
        Observable.timer(2000, TimeUnit.MILLISECONDS, Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Long>() {
                    @Override
                    public void call(Long aLong) {
//                        viewLogo.setVisibility(View.GONE);
                        viewLoading.setVisibility(View.GONE);

                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
                        finish();
                        System.gc();
                    }
                });
    }

    private void signInFirebase(String email, String password) {
        if (viewLoading.getVisibility() != View.VISIBLE)
            viewLoading.setVisibility(View.VISIBLE);
        // [START sign_in_with_email]
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Logger.d("signInWithEmail:onComplete:" + task.isSuccessful());
                        startMainActivity();

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Logger.w("signInWithEmail:failed");
//                            startMainActivity();
                            viewLoading.setVisibility(View.GONE);
                            DialogHelper.openAlertDialog(LoginActivity.this, getString(R.string.text_alert_load_device_config), getString(R.string.text_alert_system_error_please_try_agin), null);

                        }
                    }
                });
    }

    private void signUpFirebase(String email, String password) {
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Logger.d("createUserWithEmail:onComplete:");
                            if (TabletConfig.pref().isLogin() && user != null) {
                                myRef.child(user.getUid()).child("current").setValue(0);
                                myRef.child(user.getUid()).child("max").setValue(400);
                                TabletConfig.pref().setQueueMaxAvailableReserve(400);
                                TabletConfig.pref().setQueueCurrentQueueReserve(0);
                                Logger.d("setQ");
                                startMainActivity();

                            }
                        } else if (!task.isSuccessful()) {
                            Logger.w("signUpWithEmail:failed");
                            signInFirebase(edittextId.getText().toString().trim(), edittextPassword.getText().toString().trim());
                        }
                    }
                });
    }
}
