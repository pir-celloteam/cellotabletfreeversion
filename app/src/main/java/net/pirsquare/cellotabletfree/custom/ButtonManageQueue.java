package net.pirsquare.cellotabletfree.custom;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import net.pirsquare.cellotabletfree.R;


/**
 * Created by kampolleelaporn on 3/25/16.
 */
public class ButtonManageQueue extends FrameLayout {

    public enum ButtonType {
        CALL("Call", R.drawable.ic_call_queue),
        SKIP("Skip", R.drawable.ic_skip_queue),
        PRINT("Print", R.drawable.ic_print_queue),
        GET_IN("Get in", R.drawable.ic_get_in_queue),
        QUEUE("Queue", R.drawable.ic_queue_queue),
        DELETE("Delete", R.drawable.ic_delete_queue),
        FINISH("Finish", R.drawable.ic_get_in_queue),
        ARRIVED("Arrived", R.drawable.ic_get_in_queue),
        CONFIRM("OK", R.drawable.ic_correct);

        private String value;
        private int resId;

        ButtonType(String type, int resId) {
            this.value = type;
            this.resId = resId;
        }

        public String getValue() {
            return value;
        }

        public int getResId() {
            return resId;
        }
    }


    private TextView btnName;
    private ImageView icon;
    private RelativeLayout viewContainer;

    public ButtonManageQueue(Context context) {
        super(context);
        init();
    }

    public ButtonManageQueue(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ButtonManageQueue(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ButtonManageQueue(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        inflateLayout();
        initView();
    }

    private void inflateLayout() {
        inflate(getContext(), R.layout.custom_button_manage_queue, this);
    }

    private void initView() {
        btnName = (TextView) findViewById(R.id.text_button_name);
        icon = (ImageView) findViewById(R.id.ic_button);
        viewContainer = (RelativeLayout) findViewById(R.id.container);
    }

    public void setContent(ButtonType type) {
        btnName.setText(type.getValue());
        icon.setImageDrawable(ContextCompat.getDrawable(getContext(), type.resId));
    }

    public void setDisable() {
        viewContainer.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.grey_text_light));
    }

    public void textAnimation() {
        Animation shake = AnimationUtils.loadAnimation(getContext(), R.anim.shake);
        shake.setRepeatMode(Animation.REVERSE);
        shake.setDuration(80);
        shake.setRepeatCount(14);
        btnName.startAnimation(shake);
    }

}
