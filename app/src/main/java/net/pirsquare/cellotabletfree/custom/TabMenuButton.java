package net.pirsquare.cellotabletfree.custom;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import net.pirsquare.cellotabletfree.R;


/**
 * Created by kampolleelaporn on 2/29/16.
 */
public class TabMenuButton extends FrameLayout {

    private TextView textviewTabName;
    private TextView textviewCountQueue;
    private RelativeLayout container;

    public TabMenuButton(Context context) {
        super(context);
        init();
    }

    public TabMenuButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TabMenuButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public TabMenuButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        inflateLayout();
        initView();
    }

    private void inflateLayout() {
        inflate(getContext(), R.layout.custom_tab_menu_button, this);
    }

    private void initView() {
        container = (RelativeLayout) findViewById(R.id.tab_container);
        textviewTabName = (TextView) findViewById(R.id.textview_tab_name);
        textviewCountQueue = (TextView) findViewById(R.id.textview_tab_no_queue);
    }

    public void setTabName(String text) {
        textviewTabName.setText(text);
    }

    public void setSelect(boolean isSelect) {
        container.setSelected(isSelect);
    }

    public void setNoQueue(int number) {
        textviewCountQueue.setText(String.valueOf(number));
    }
}
