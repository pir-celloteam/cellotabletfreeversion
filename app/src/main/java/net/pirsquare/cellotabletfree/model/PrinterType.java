package net.pirsquare.cellotabletfree.model;

/**
 * Created by kung on 8/8/16.
 */
public enum PrinterType {
    MOBILE(1), POS(2);

    private int value;

    PrinterType(int i) {
        this.value = i;
    }

    public int getValue() {
        return value;
    }
}
