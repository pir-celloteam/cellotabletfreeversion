package net.pirsquare.cellotabletfree.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by kung on 8/5/16.
 */
public enum TabletType {

    @SerializedName("counter_management")
    COUNTER_MANAGEMENT("counter_management"),
    @SerializedName("queue_management")
    QUEUE_MANAGEMENT("queue_management"),
    @SerializedName("external_counter")
    EXTERNAL_COUNTER("external_counter"),
    @SerializedName("kiosk")
    KIOSK("kiosk");

    private String value;

    TabletType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static TabletType getType(String value) {
        switch (value) {
            case "counter_management":
                return COUNTER_MANAGEMENT;
            case "external_counter":
                return EXTERNAL_COUNTER;
            case "kiosk":
                return KIOSK;
            default:
                return QUEUE_MANAGEMENT;
        }
    }
}
