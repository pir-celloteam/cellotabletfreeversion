package net.pirsquare.cellotabletfree.model;

/**
 * Created by kung on 1/27/17.
 */

public class ReservedQueue {
    public int current;
    public int max;

    public ReservedQueue(int current, int max) {
        this.current = current;
        this.max = max;
    }

    public int getCurrent() {
        return current;
    }

    public void setCurrent(int current) {
        this.current = current;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }
}
