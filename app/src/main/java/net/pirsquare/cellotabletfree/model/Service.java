package net.pirsquare.cellotabletfree.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by kung on 8/11/16.
 */
public class Service implements Parcelable {
    private String serviceId;
    private String serviceName;
    private boolean isAllowCapacity;
    private int sortNumber;
    private boolean isEnable;
    private int minCapacity;
    private int maxCapacity;
    private boolean isAllowBooking;

    public Service(String serviceId, String serviceName, boolean isAllowCapacity, int sortNumber, boolean isAllowBooking) {
        this.serviceId = serviceId;
        this.serviceName = serviceName;
        this.isAllowCapacity = isAllowCapacity;
        this.sortNumber = sortNumber;
        this.isAllowBooking = isAllowBooking;
    }

    public Service(String serviceId, String serviceName, boolean isAllowCapacity, int sortNumber, int minCapacity, int maxCapacity, boolean isAllowBooking) {
        this.serviceId = serviceId;
        this.serviceName = serviceName;
        this.isAllowCapacity = isAllowCapacity;
        this.sortNumber = sortNumber;
        this.minCapacity = minCapacity;
        this.maxCapacity = maxCapacity;
        this.isAllowBooking = isAllowBooking;
    }

    public String getServiceId() {
        return serviceId;
    }

    public String getServiceName() {
        return serviceName;
    }


    public boolean isAllowCapacity() {
        return isAllowCapacity;
    }


    public int getSortNumber() {
        return sortNumber;
    }


    public boolean isEnable() {
        return isEnable;
    }

    public void setEnable(boolean enable) {
        isEnable = enable;
    }

    public int getMinCapacity() {
        return minCapacity;
    }

    public int getMaxCapacity() {
        return maxCapacity;
    }

    public boolean isAllowBooking() {
        return isAllowBooking;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.serviceId);
        dest.writeString(this.serviceName);
        dest.writeByte(this.isAllowCapacity ? (byte) 1 : (byte) 0);
        dest.writeInt(this.sortNumber);
        dest.writeByte(this.isEnable ? (byte) 1 : (byte) 0);
        dest.writeInt(this.minCapacity);
        dest.writeInt(this.maxCapacity);
        dest.writeByte(this.isAllowBooking ? (byte) 1 : (byte) 0);
    }

    protected Service(Parcel in) {
        this.serviceId = in.readString();
        this.serviceName = in.readString();
        this.isAllowCapacity = in.readByte() != 0;
        this.sortNumber = in.readInt();
        this.isEnable = in.readByte() != 0;
        this.minCapacity = in.readInt();
        this.maxCapacity = in.readInt();
        this.isAllowBooking = in.readByte() != 0;
    }

    public static final Creator<Service> CREATOR = new Creator<Service>() {
        @Override
        public Service createFromParcel(Parcel source) {
            return new Service(source);
        }

        @Override
        public Service[] newArray(int size) {
            return new Service[size];
        }
    };
}
