package net.pirsquare.cellotabletfree.model;

/**
 * Created by kampolleelaporn on 3/9/16.
 */
public enum QueueState {
    QUEUE("queue", 0), HOLD("hold", 1), GET_IN("getIn", 2);

    private String value;
    private int i;

    QueueState(String value, int i) {
        this.value = value;
        this.i = i;
    }

    public String getValue() {
        return value;
    }

    public int getValueInteger() {
        return i;
    }

    public static QueueState getType(String type) {
        switch (type) {
            case "queue":
                return QUEUE;
            case "hold":
                return HOLD;
            case "getIn":
                return GET_IN;
        }
        return null;
    }

    public static QueueState getType(int type) {
        switch (type) {
            case 0:
                return QUEUE;
            case 1:
                return HOLD;
            case 2:
                return GET_IN;
        }
        return null;
    }
}
