package net.pirsquare.cellotabletfree.model;

import com.google.gson.annotations.SerializedName;

public enum QueueAction {
    @SerializedName("reserved")
    RESERVE("reserved"),
    @SerializedName("accepted")
    ACCEPT("accepted"),
    @SerializedName("call")
    CALL("call"),
    @SerializedName("hold")
    HOLD("hold"),
    @SerializedName("claim")
    CLAIM("claim"),
    @SerializedName("cancel")
    CANCEL("cancel"),
    @SerializedName("cancelled")
    CANCELLED("cancelled"),
    @SerializedName("deleted")
    DELETED("deleted"),
    @SerializedName("expired")
    EXPIRED("expired");

    private final String value;

    QueueAction(String accept) {
        this.value = accept;
    }

    public String getValue() {
        return value;
    }

    public static QueueAction getQueueStatus(String value) {
        switch (value) {
            case "reserved":
                return RESERVE;
            case "call":
                return CALL;
            case "hold":
                return HOLD;
            case "claim":
                return CLAIM;
            case "accepted":
                return ACCEPT;
            case "cancel":
                return CANCEL;
            case "cancelled":
                return CANCELLED;
            case "deleted":
                return DELETED;
            case "expired":
                return EXPIRED;
        }
        return null;
    }
}