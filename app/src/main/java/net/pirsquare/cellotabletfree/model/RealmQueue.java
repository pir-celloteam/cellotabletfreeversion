package net.pirsquare.cellotabletfree.model;


import net.pirsquare.cellotabletfree.manager.DateHelper;

import org.joda.time.DateTime;
import org.parceler.Parcel;

import java.util.Date;
import java.util.HashMap;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by kampolleelaporn on 3/17/16.
 */
@Parcel(value = Parcel.Serialization.BEAN, analyze = {RealmQueue.class})
public class RealmQueue extends RealmObject {

    @PrimaryKey
    private String _id;
    private String orderNumber;
    private String refCode;
    private String branchId;
    private String group;
    private String index;
    private int capacity;
    private String state;
    private String action;
    private String type;
    private int remainStart;
    private int remain;
    private Date createdAt;
    private Date endAt;
    private boolean isActive;
    private RealmUser user;
    private String serviceId;
    private String serviceName;
    private String destination;
    private boolean isReset;
    private boolean isCancel;

    public String getQueueId() {
        return _id;
    }

    public void setQueueId(String queueId) {
        this._id = queueId;
    }

    public String getQueueGroup() {
        return group;
    }

    public void setQueueGroup(String queueGroup) {
        this.group = queueGroup;
    }

    public String getQueueIndex() {
        return index;
    }

    public void setQueueIndex(String queueIndex) {
        this.index = queueIndex;
    }

    public String getQueueState() {
        return state;
    }

    public void setQueueState(String queueState) {
        this.state = queueState;
    }

    public int getQueueRemain() {
        return remain;
    }

    public void setQueueRemain(int queueRemain) {
        this.remain = queueRemain;
    }

    public String getTimeReserve() {
        return DateHelper.formatFully.print(new DateTime(createdAt));
    }

    public String getTimeReserveQueue() {
        return DateHelper.formatTime.print(new DateTime(createdAt));
    }

    public void setTimeReserve(String timeReserve) {
        this.createdAt = new DateTime(timeReserve).toDate();
    }

    public int getNumSeat() {
        return capacity;
    }

    public void setNumSeat(int numSeat) {
        this.capacity = numSeat;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public String getQueueAction() {
        return action;
    }

    public void setQueueAction(String queueAction) {
        this.action = queueAction;
        this.endAt = DateHelper.getCurrentDate();
    }

    public String getQueueType() {
        return type;
    }

    public void setQueueType(String queueType) {
        this.type = queueType;
    }

    public RealmUser getUser() {
        return user;
    }

    public void setUser(RealmUser user) {
        this.user = user;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setRemainStart(int remainStart) {
        this.remainStart = remainStart;
    }


    public boolean isReset() {
        return isReset;
    }

    public void setReset(boolean reset) {
        isReset = reset;
    }

    public Date getEndAt() {
        return endAt;
    }


    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public boolean isCancel() {
        return isCancel;
    }

    public void setCancel(boolean cancel) {
        isCancel = cancel;
    }

    public String getRefCode() {
        return refCode;
    }

    public void setRefCode(String refCode) {
        this.refCode = refCode;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public HashMap<String, Object> toJson() {
        HashMap<String, Object> obj = new HashMap<>();
        obj.put("_id", _id);
        obj.put("branchId", branchId);
        obj.put("group", group);
        obj.put("index", index);
        obj.put("capacity", capacity);
        obj.put("status", action);
        obj.put("type", type);
        obj.put("remainAtStart", remainStart);
        obj.put("remain", remain);
        obj.put("createdAt", createdAt);
        obj.put("endAt", endAt);
        obj.put("serviceId", serviceId);
        obj.put("serviceName", serviceName);
        obj.put("user", user.toJson());
        return obj;
    }

}
