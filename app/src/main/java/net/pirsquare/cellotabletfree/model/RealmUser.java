package net.pirsquare.cellotabletfree.model;

import org.parceler.Parcel;

import java.util.HashMap;

import io.realm.RealmObject;

/**
 * Created by kampolleelaporn on 3/30/16.
 */
@Parcel(value = Parcel.Serialization.BEAN, analyze = {RealmUser.class})
public class RealmUser extends RealmObject {
    private String _id;
    private String name;
    private String telephone;
    private String local;

    public String getId() {
        return _id;
    }

    public void setId(String _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public HashMap<String, Object> toJson() {
        HashMap<String, Object> obj = new HashMap<>();
        obj.put("_id", _id);
        obj.put("name", name);
        obj.put("telephone", telephone);
        return obj;
    }
}
