package net.pirsquare.cellotabletfree.model;

public enum QueueType {
    PAPER("paper"), MOBILE("mobile");

    private String value;

    QueueType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}