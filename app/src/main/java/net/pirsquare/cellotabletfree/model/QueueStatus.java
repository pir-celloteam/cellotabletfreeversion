package net.pirsquare.cellotabletfree.model;

/**
 * Created by kung on 10/20/16.
 */

public enum QueueStatus {
    QUEUE_STATUS_QUEUE("queue"),
    QUEUE_STATUS_HOLD("hold"),
    QUEUE_STATUS_GETIN("getIn"),
    QUEUE_STATUS_DELETE("delete");


    private String value;

    QueueStatus(String status) {
        this.value = status;
    }

    public String getValue() {
        return value;
    }

    public static QueueStatus type(String key) {
        switch (key) {
            case "queue":
                return QUEUE_STATUS_QUEUE;
            case "hold":
                return QUEUE_STATUS_HOLD;
            case "getIn":
                return QUEUE_STATUS_GETIN;
            case "delete":
                return QUEUE_STATUS_DELETE;
        }
        return null;
    }
}
