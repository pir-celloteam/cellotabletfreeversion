package net.pirsquare.cellotabletfree.model;

import org.parceler.Parcel;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by kampolleelaporn on 3/21/16.
 */
@Parcel(value = Parcel.Serialization.BEAN, analyze = {RealmGroup.class})
public class RealmGroup extends RealmObject {

    @PrimaryKey
    private String groupName;
    private int minSeat;
    private int maxSeat;
    private int resColor;
    private String serviceId;
    private String serviceName;
    private boolean isAllowCapacity;
    private boolean isAllowBooking;

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public void setMinSeat(int minSeat) {
        this.minSeat = minSeat;
    }

    public void setMaxSeat(int maxSeat) {
        this.maxSeat = maxSeat;
    }

    public void setResColor(int resColor) {
        this.resColor = resColor;
    }

    public String getGroupName() {
        return groupName;
    }

    public int getMinSeat() {
        return minSeat;
    }

    public int getMaxSeat() {
        return maxSeat;
    }

    public int getResColor() {
        return resColor;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }


    public boolean isAllowCapacity() {
        return isAllowCapacity;
    }

    public void setAllowCapacity(boolean allowCapacity) {
        isAllowCapacity = allowCapacity;
    }

    public void setAllowBooking(boolean allowBooking) {
        isAllowBooking = allowBooking;
    }

    public boolean isAllowBooking() {
        return isAllowBooking;
    }
}
