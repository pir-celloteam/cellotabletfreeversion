package net.pirsquare.cellotabletfree.dialog;


import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextClock;
import android.widget.TextView;

import com.lb.auto_fit_textview.AutoResizeTextView;

import net.pirsquare.cellotabletfree.R;
import net.pirsquare.cellotabletfree.custom.FullscreenDialogFragment;
import net.pirsquare.cellotabletfree.custom.OnSingleClickListener;
import net.pirsquare.cellotabletfree.manager.QueueManager;
import net.pirsquare.cellotabletfree.util.ConvertDesity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by thanawat on 8/23/15 AD.
 * Project : MindShot.
 */
public class ReserveQueueDialogFragmentViewController extends FullscreenDialogFragment {


    @BindView(R.id.textview_queue_name)
    TextView textviewQueueName;
    @BindView(R.id.textclock)
    TextClock textclock;
    @BindView(R.id.view_group_color)
    LinearLayout viewGroupColor;
    @BindView(R.id.textview_customer_name)
    EditText editTextCustomerName;
    @BindView(R.id.textview_customer_phone)
    EditText editTextCustomerNameCustomerPhone;
    @BindView(R.id.textview_queue_remain)
    TextView textviewQueueRemain;
    @BindView(R.id.view_container_btn)
    LinearLayout viewContainerBtn;
    @BindView(R.id.iv_customer_name)
    ImageView ivCustomerName;
    @BindView(R.id.iv_customer_phone)
    ImageView ivCustomerPhone;

    private Dialog dialog;

    private ReserveListener listener;

    private ArrayList<TextView> listBtn;

    public static ReserveQueueDialogFragmentViewController newInstance() {
        ReserveQueueDialogFragmentViewController fragment = new ReserveQueueDialogFragmentViewController();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public ReserveQueueDialogFragmentViewController() {
        // Required empty public constructor
    }

    public void setListener(ReserveListener listener) {
        this.listener = listener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_reserve_queue, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        int animationId = getEnterAnimation();

        if (animationId != 0) {
            Animation animation = AnimationUtils.loadAnimation(view.getContext(), animationId);
            view.startAnimation(animation);
        }

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        return dialog;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        /**
         * Add Function
         */
        textviewQueueName.setText(String.format("%s%d", "A", QueueManager.getInstance().getCurrentQueueNumber() + 1));
        textviewQueueRemain.setText(String.valueOf(QueueManager.getInstance().getListQueue().size()));
        addButton();
    }

    private void addButton() {
        listBtn = new ArrayList<>();
        int size = 10;
        viewContainerBtn.removeAllViews();
        viewContainerBtn.setWeightSum(size);
        for (int i = 0; i < size; i++) {
            final AutoResizeTextView btn = new AutoResizeTextView(getActivity());
            btn.setText(String.format("%d", i + 1));
            btn.setGravity(Gravity.CENTER_HORIZONTAL);
            btn.setMaxLines(1);
            btn.setEllipsize(TextUtils.TruncateAt.END);
            btn.setId(i + 1);
            btn.setTextColor(ContextCompat.getColor(getContext(), R.color.white));
            btn.setTextSize(26f);
            btn.setPadding(
                    (int) ConvertDesity.convertDpToPixel(10f, getContext()),
                    (int) ConvertDesity.convertDpToPixel(25f, getContext()),
                    (int) ConvertDesity.convertDpToPixel(10f, getContext()),
                    (int) ConvertDesity.convertDpToPixel(25f, getContext()));
            btn.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.red_ci));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.width = 0;
            params.weight = 1;
            params.setMargins(0, 0, i != size - 1 ? (int) ConvertDesity.convertDpToPixel(1f, getContext()) : 0, 0);
            btn.setLayoutParams(params);
            btn.setOnClickListener(new OnSingleClickListener() {
                @Override
                public void onSingleClick(View v) {
                    onClickTab(v);
                }
            });

            listBtn.add(btn);
            viewContainerBtn.addView(btn);
        }

    }

    private void onClickTab(View view) {
        if (listener != null)
            listener.onClickReserveQueue(editTextCustomerName.getText().toString(), editTextCustomerNameCustomerPhone.getText().toString(), view.getId(), QueueManager.getInstance().getGroupAvailable().getServiceId(), QueueManager.getInstance().getGroupAvailable().getServiceName());
        close();
    }

    @OnClick(R.id.iv_customer_name)
    void onClickIvCustomerName() {
        editTextCustomerName.setFocusable(true);
    }

    @OnClick(R.id.iv_customer_phone)
    void onClickIvCustomerPhone() {
        editTextCustomerNameCustomerPhone.setFocusable(true);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @OnClick(R.id.container)
    public void onClickContainer() {
        close();
    }

    public interface ReserveListener {
        void onClickReserveQueue(String name, String telephone, int noSeat, String serviceId, String serviceName);
    }


}
