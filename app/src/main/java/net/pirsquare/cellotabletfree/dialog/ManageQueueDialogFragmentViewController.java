package net.pirsquare.cellotabletfree.dialog;


import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.google.firebase.analytics.FirebaseAnalytics;

import net.pirsquare.cellotabletfree.R;
import net.pirsquare.cellotabletfree.application.App;
import net.pirsquare.cellotabletfree.custom.ButtonManageQueue;
import net.pirsquare.cellotabletfree.custom.FullscreenDialogFragment;
import net.pirsquare.cellotabletfree.custom.OnSingleClickListener;
import net.pirsquare.cellotabletfree.main.MainActivity;
import net.pirsquare.cellotabletfree.manager.DateHelper;
import net.pirsquare.cellotabletfree.manager.DialogHelper;
import net.pirsquare.cellotabletfree.manager.PrinterManager;
import net.pirsquare.cellotabletfree.manager.QueueManager;
import net.pirsquare.cellotabletfree.manager.TabletConfig;
import net.pirsquare.cellotabletfree.model.QueueState;
import net.pirsquare.cellotabletfree.model.QueueStatus;
import net.pirsquare.cellotabletfree.model.QueueType;
import net.pirsquare.cellotabletfree.model.RealmQueue;
import net.pirsquare.cellotabletfree.util.Utils;

import org.joda.time.DateTime;
import org.joda.time.Hours;
import org.joda.time.Minutes;
import org.parceler.Parcels;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by thanawat on 8/23/15 AD.
 * Project : MindShot.
 */
public class ManageQueueDialogFragmentViewController extends FullscreenDialogFragment {


    @BindView(R.id.textview_queue_name)
    TextView textviewQueueName;
    @BindView(R.id.textview_no_seat)
    TextView textviewNoSeat;
    @BindView(R.id.textview_time_in)
    TextView textviewTimeIn;
    @BindView(R.id.textview_queue_waiting)
    TextView textviewQueueWaiting;
    @BindView(R.id.textview_queue_remain)
    TextView textviewQueueRemain;
    @BindView(R.id.view_background_color)
    RelativeLayout viewBackgroundColor;
    @BindView(R.id.textview_customer_name)
    EditText textviewCustomerName;
    @BindView(R.id.textview_customer_phone)
    EditText textviewCustomerPhone;
    @BindView(R.id.btn1)
    ButtonManageQueue btn1;
    @BindView(R.id.btn2)
    ButtonManageQueue btn2;
    @BindView(R.id.btn3)
    ButtonManageQueue btn3;
    @BindView(R.id.btn4)
    ButtonManageQueue btn4;
    @BindView(R.id.btn5)
    ButtonManageQueue btn5;
    @BindView(R.id.view_confirm)
    LinearLayout viewConfirm;
    private Dialog dialog;
    private RealmQueue queue;

    private QueueState type;

    public static ManageQueueDialogFragmentViewController newInstance(RealmQueue queue, QueueState type) {
        ManageQueueDialogFragmentViewController fragment = new ManageQueueDialogFragmentViewController();
        Bundle args = new Bundle();
        args.putParcelable("queue", Parcels.wrap(queue));
        args.putSerializable("type", type);
        fragment.setArguments(args);
        return fragment;
    }

    public ManageQueueDialogFragmentViewController() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        queue = Parcels.unwrap(getArguments().getParcelable("queue"));
        type = (QueueState) getArguments().getSerializable("type");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_manage_queue, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        int animationId = getEnterAnimation();

        if (animationId != 0) {
            Animation animation = AnimationUtils.loadAnimation(view.getContext(), animationId);
            view.startAnimation(animation);
        }
        setupUI(view);

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        return dialog;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        /**
         * Add Function3
         */
        textviewQueueName.setText(String.format("%s%s", queue.getQueueGroup(), queue.getQueueIndex()));
        textviewCustomerName.setText(TextUtils.isEmpty(queue.getUser().getName()) ? "Guest" : queue.getUser().getName());
        textviewCustomerPhone.setText(TextUtils.isEmpty(queue.getUser().getTelephone()) ? "" : queue.getUser().getTelephone());
        textviewNoSeat.setText(String.valueOf(queue.getNumSeat()));
        textviewQueueRemain.setText(String.valueOf(queue.getQueueRemain()));
        DateTime dt = DateHelper.formatFully.parseDateTime(queue.getTimeReserve());
        textviewTimeIn.setText(String.format("%d:%d", dt.getHourOfDay(), dt.getMinuteOfHour()));
        int hours = Hours.hoursBetween(dt, new DateTime()).getHours();
        int minutes = Minutes.minutesBetween(dt, new DateTime()).getMinutes() % 60;
        textviewQueueWaiting.setText(String.format("%d:%s", hours, minutes < 10 ? "0" + minutes : minutes));
        btn1.setContent(ButtonManageQueue.ButtonType.CALL);
        if (queue.getQueueType().equalsIgnoreCase(QueueType.MOBILE.getValue())) {
            btn1.setOnClickListener(new OnSingleClickListener() {
                @Override
                public void onSingleClick(View v) {
                    callQueue();
                    btn1.textAnimation();
                }
            });
        } else {
            btn1.setDisable();
        }
        btn3.setContent(ButtonManageQueue.ButtonType.PRINT);
        if (!TabletConfig.pref().isEnablePrinter()) {
            btn3.setDisable();
        }
        btn3.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (TabletConfig.pref().isEnablePrinter())
                    printQueue();
                else {
                    /**
                     * Alert Promotion
                     */
                    DialogHelper.openDialogUsePrinter(getActivity());
                }
            }
        });


        switch (type) {
            case QUEUE:
                btn2.setContent(ButtonManageQueue.ButtonType.SKIP);
                btn4.setContent(ButtonManageQueue.ButtonType.GET_IN);
                btn4.setOnClickListener(new OnSingleClickListener() {
                    @Override
                    public void onSingleClick(View v) {
                        toGetIn();
                    }
                });
                if (!TabletConfig.pref().isAvaiableHold()) {
                    btn2.setDisable();
                } else {
                    btn2.setOnClickListener(new OnSingleClickListener() {
                        @Override
                        public void onSingleClick(View v) {
                            viewConfirm.setVisibility(viewConfirm.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
                        }
                    });
                    btn5.setContent(ButtonManageQueue.ButtonType.CONFIRM);
                    btn5.setOnClickListener(new OnSingleClickListener() {
                        @Override
                        public void onSingleClick(View v) {
                            toHold();
                        }
                    });
                }
                break;
            case HOLD:
                btn2.setContent(ButtonManageQueue.ButtonType.QUEUE);
                btn4.setContent(ButtonManageQueue.ButtonType.GET_IN);
                btn2.setDisable();
                btn4.setOnClickListener(new OnSingleClickListener() {
                    @Override
                    public void onSingleClick(View v) {
                        toGetIn();
                    }
                });
                break;
            case GET_IN:
                btn2.setContent(ButtonManageQueue.ButtonType.SKIP);
                btn2.setDisable();
                btn4.setContent(ButtonManageQueue.ButtonType.GET_IN);
                btn4.setDisable();
                break;
        }

        if (!queue.isActive()) {
            /**
             * Queue inActive
             * Can delete
             */
            btn1.setOnClickListener(null);
            btn2.setOnClickListener(null);
            btn3.setOnClickListener(null);
            btn1.setDisable();
            btn2.setDisable();
            btn3.setDisable();
            btn4.setContent(ButtonManageQueue.ButtonType.DELETE);
            btn4.setOnClickListener(new OnSingleClickListener() {
                @Override
                public void onSingleClick(View v) {
                    deleteInactive();
                }
            });
        }

        textviewCustomerName.setFocusable(false);
        textviewCustomerPhone.setFocusable(false);
        textviewCustomerName.setFocusableInTouchMode(false);
        textviewCustomerPhone.setFocusableInTouchMode(false);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        /**
         * For Add Call Back
         */
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void close() {
        super.close();
    }

    @OnClick(R.id.container)
    public void onClickContainer() {
        hideSoftKeyboard(getActivity());
        close();
    }

    private void toHold() {
        QueueManager.getInstance().changeStateQueue(queue, QueueStatus.QUEUE_STATUS_HOLD);
        close();
    }

    private void toGetIn() {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "ManageQueue");
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Queue Get in");
        App.mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);

        QueueManager.getInstance().changeStateQueue(queue, QueueStatus.QUEUE_STATUS_GETIN);
        close();
    }

    private void toQueue() {
        QueueManager.getInstance().changeStateQueue(queue, QueueStatus.QUEUE_STATUS_QUEUE);
        close();
    }

    private void deleteInactive() {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "ManageQueue");
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Queue Delete");
        App.mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);

        QueueManager.getInstance().deleteInactiveQueue(queue);
        close();
    }

    private void callQueue() {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "ManageQueue");
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Queue Call");
        App.mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);

        QueueManager.getInstance().callQueue(queue);
    }

    private void printQueue() {
        if (TabletConfig.pref().isEnablePrinter() && TabletConfig.pref().isAutoPrint() && PrinterManager.getInstance() != null) {
            PrinterManager.getInstance().printQueue(3
                    , queue.getTimeReserve()
                    , String.format("%s%s", queue.getQueueGroup(), queue.getQueueIndex())
                    , String.format("%d Seats", queue.getNumSeat())
                    , Utils.getCode(queue), queue.getQueueRemain(), queue.getServiceName(), queue.getRefCode());
            close();
        } else {
            close();
        }

    }

    private void setupUI(View view) {
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    hideSoftKeyboard(getActivity());
                    return false;
                }
            });
        }

        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView);
            }
        }
    }

    private void hideSoftKeyboard(Activity activity) {
        if (activity.getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromInputMethod(activity.getCurrentFocus().getWindowToken(), 0);
        }
    }


}
