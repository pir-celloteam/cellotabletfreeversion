package net.pirsquare.cellotabletfree.manager;

import net.pirsquare.cellotabletfree.main.Constant;

import org.hashids.Hashids;

/**
 * Created by kampolleelaporn on 3/22/16.
 */
public class EncodeHelper {
    private Hashids hashids;
    private static EncodeHelper mInstance;

    public EncodeHelper() {
        hashids = new Hashids(Constant.SECRET_KEY, 3, "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ123456789");
    }

    public static EncodeHelper getmInstance() {
        if (mInstance == null)
            mInstance = new EncodeHelper();
        return mInstance;
    }

    public String getKey(long time) {
        return hashids.encode(time, (int) (Math.random() * 999));
    }

    public String getRef(String orderQueue, int day, String branchIndex) {
        return hashids.encode(Integer.valueOf(orderQueue), day, Integer.valueOf(branchIndex));
    }

    public String decodeKey(String key) {
        long[] detail = hashids.decode(key);
        return String.valueOf(detail[0]);
    }
}
