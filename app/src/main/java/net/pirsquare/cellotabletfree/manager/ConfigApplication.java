package net.pirsquare.cellotabletfree.manager;

import ru.noties.spg.anno.SPGPreference;

/**
 * Created by kampolleelaporn on 4/7/16.
 */
@SPGPreference(isSingleton = true)
public class ConfigApplication {

    long lastestSentReport;

    public ConfigApplication() {
    }

    public static ConfigApplicationPreference prefs() {
        return ConfigApplicationPreference.getInstance();
    }

}
