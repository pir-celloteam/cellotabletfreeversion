package net.pirsquare.cellotabletfree.manager;

import android.app.Activity;
import android.util.Log;

import net.pirsquare.sewoo.PrintManager;

/**
 * Created by kampolleelaporn on 4/4/16.
 */
public class PrinterManager {

    private static PrinterManager mInstance;
    private PrintManager printManager;

    private Activity mAvActivity;
    private boolean isMobilePrinter;


    public PrinterManager() {

    }

    public void init(Activity mAvActivity) {
        this.mAvActivity = mAvActivity;

        isMobilePrinter = true;
        printManager = new PrintManager(mAvActivity, new PrintManager.PrinterListener() {
            @Override
            public void onConnect() {
                Log.e("PrinterManager", "Connect");
                RxBus.getInstance().send(new PrinterEvent(true));
            }

            @Override
            public void onDisconnect() {
                Log.e("PrinterManager", "DisConnect");
                RxBus.getInstance().send(new PrinterEvent(false));
            }
        });
    }

    public static PrinterManager getInstance() {
        if (mInstance == null)
            mInstance = new PrinterManager();
        return mInstance;
    }


    public void printQueue(int type,
                           String time_stamp, String queue_id,
                           String seat_amount, String qr, int queue_remain, String serviceName, String refCode) {
        printManager.printQueue(type, DateHelper.getTimetoShowInQueuePaper(time_stamp), queue_id, seat_amount, qr, queue_remain, refCode);

    }

    public void checkPrint(PrintManager.CheckPrintListener listener) {
        printManager.checkPrint(listener);
    }

    public void disconnect() {
        if (printManager != null) {
            printManager.dispose();
            printManager = null;
        }
        mInstance = null;
    }

    public static class PrinterEvent {
        public boolean isConnect;

        public PrinterEvent(boolean isConnect) {
            this.isConnect = isConnect;
        }
    }

}
