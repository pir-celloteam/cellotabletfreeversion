package net.pirsquare.cellotabletfree.manager;


import net.pirsquare.cellotabletfree.service.register.bean.RegisParam;

/**
 * Created by kung on 6/10/16.
 */
public class RegisterHelper {
    private static RegisterHelper mInstance;
    private String username;
    private String serialKey;
    private String printerMacAddress;
    private String tvMacAddress;
    private String tabletMacAddress;
    private String appVersion;
    private String osVersion;

    public static RegisterHelper getInstance() {
        if (mInstance == null)
            mInstance = new RegisterHelper();
        return mInstance;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSerialKey() {
        return serialKey;
    }

    public void setSerialKey(String serailKey) {
        this.serialKey = serailKey;
    }

    public String getPrinterMacAddress() {
        return printerMacAddress;
    }

    public void setPrinterMacAddress(String printerMacAddress) {
        this.printerMacAddress = printerMacAddress;
    }

    public String getTvMacAddress() {
        return tvMacAddress;
    }

    public void setTvMacAddress(String tvMacAddress) {
        this.tvMacAddress = tvMacAddress;
    }

    public String getTabletMacAddress() {
        return tabletMacAddress;
    }

    public void setTabletMacAddress(String tabletMacAddress) {
        this.tabletMacAddress = tabletMacAddress;
    }

    public void setAppDetail(String appVersion, String osVersion) {
        this.appVersion = appVersion;
        this.osVersion = osVersion;
    }

    public RegisParam getParam() {
        RegisParam param = new RegisParam();
        param.setPassword(serialKey);
        param.setUsername(username);
        param.setTvMacAddress(tvMacAddress);
        param.setPrinterMacAddress(printerMacAddress);
        param.setTabletMacAddress(tabletMacAddress);
        param.setDeviceDetail(appVersion, osVersion);
        return param;
    }
}
