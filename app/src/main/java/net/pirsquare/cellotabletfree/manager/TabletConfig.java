package net.pirsquare.cellotabletfree.manager;

import ru.noties.spg.anno.SPGKey;
import ru.noties.spg.anno.SPGPreference;

/**
 * Created by kung on 6/13/16.
 */
@SPGPreference(isSingleton = true)
public class TabletConfig {

    boolean isBluetoothTV;
    boolean isLogin;
    String idTablet;
    String tvMacAddress;
    String printerMacAddress;
    String tabletMacAddress;
    String user;
    String serialNumber;
    String userId;

    @SPGKey(defaultValue = "0")
    String branchIndex;
    String logo;
    String branchId;
    String branchName;
    int holdExpire;
    boolean isAvaiableHold;

    String s3BucketName;
    String s3KeyDirectory;

    String printerTopImagePath;
    String printerBottomImagePath;
    @SPGKey(defaultValue = "true")
    boolean isAutoPrint;
    @SPGKey(defaultValue = "restaurant")
    String softwareType;

    String appType;
    int printerTypeId;
    String printerIpAddress;
    String serviceData;
    boolean isServiceChange;

    boolean isResetStructure;

    boolean isEnableCounter;

    boolean isOpenMobileReserve;

    boolean isEnablePrinter;

    boolean isAvailableCreateQueue;


    int queueMaxAvailableReserve;
    int queueCurrentQueueReserve;

    @SPGKey(defaultValue = "true")
    boolean isAllowGroupManagement;

    public TabletConfig() {
    }

    public static TabletConfigPreference pref() {
        return TabletConfigPreference.getInstance();
    }
}
