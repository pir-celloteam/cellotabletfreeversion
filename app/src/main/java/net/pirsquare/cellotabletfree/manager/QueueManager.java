package net.pirsquare.cellotabletfree.manager;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import net.pirsquare.cellotabletfree.R;
import net.pirsquare.cellotabletfree.main.MainActivity;
import net.pirsquare.cellotabletfree.model.QueueAction;
import net.pirsquare.cellotabletfree.model.QueueState;
import net.pirsquare.cellotabletfree.model.QueueStatus;
import net.pirsquare.cellotabletfree.model.QueueType;
import net.pirsquare.cellotabletfree.model.RealmGroup;
import net.pirsquare.cellotabletfree.model.RealmQueue;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmAsyncTask;
import io.realm.RealmResults;
import java8.util.function.Consumer;
import java8.util.stream.StreamSupport;

import static net.pirsquare.cellotabletfree.model.QueueStatus.QUEUE_STATUS_GETIN;
import static net.pirsquare.cellotabletfree.model.QueueStatus.QUEUE_STATUS_HOLD;
import static net.pirsquare.cellotabletfree.model.QueueStatus.QUEUE_STATUS_QUEUE;

/**
 * Created by kung on 10/20/16.
 */

public class QueueManager {
    public static String TAG = "QueueManager";
    private static QueueManager mInstance;
    private Activity mActivity;

    private HashMap<String, RealmQueue> hashmapQueue;
    private ArrayList<RealmQueue> listQueue;
    private ArrayList<RealmQueue> listSkip;
    private ArrayList<RealmQueue> listGetin;
    private RealmGroup groupAvailable;

    private LoadQueueListener loadQueueListener;
    private static int counterQueue;
    private RealmAsyncTask transaction;


    public void init(Activity mActivity, LoadQueueListener loadQueueListener) {
        this.mActivity = mActivity;
        this.loadQueueListener = loadQueueListener;
        hashmapQueue = new HashMap<>();
        listQueue = new ArrayList<>();
        listSkip = new ArrayList<>();
        listGetin = new ArrayList<>();

        groupAvailable = new RealmGroup();
        groupAvailable.setServiceId("P1XdBRdCoq");
        groupAvailable.setAllowCapacity(true);
        groupAvailable.setMinSeat(1);
        groupAvailable.setMaxSeat(10);
        groupAvailable.setGroupName("A");
        groupAvailable.setResColor(R.color.red_ci);

        mockDate();

        if (ConfigApplication.prefs().getLastestSentReport() == -1L) {
            ConfigApplication.prefs().setLastestSentReport(DateHelper.getMilliseconds());
        }
    }

    public static QueueManager getInstance() {
        if (mInstance == null)
            mInstance = new QueueManager();
        return mInstance;
    }

    public QueueManager() {

    }

    private void mockDate() {
        Log.e(TAG, "Start load group");

        RealmResults<RealmQueue> results = Realm.getDefaultInstance().where(RealmQueue.class).equalTo("isReset", false).equalTo("group", "A").findAll();
        counterQueue = results.size();
        loadQueueFromDB();
    }

    public void loadQueueFromDB() {
        RealmResults<RealmQueue> result = Realm.getDefaultInstance().where(RealmQueue.class).equalTo("isActive", true).equalTo("isReset", false).findAll();
        if (result.size() > 0) {
            StreamSupport.stream(result)
                    .forEach(new Consumer<RealmQueue>() {
                        @Override
                        public void accept(RealmQueue realmQueue) {
                            RealmQueue tempQueue = Realm.getDefaultInstance().copyFromRealm(realmQueue);
                            hashmapQueue.put(tempQueue.getQueueId(), tempQueue);
                            Log.e("Queue", tempQueue.getQueueId() + " : " + tempQueue.getQueueGroup() + tempQueue.getQueueIndex());
                            switch (QueueStatus.type(tempQueue.getQueueState())) {
                                case QUEUE_STATUS_QUEUE:
                                    listQueue.add(tempQueue);
                                    break;
                                case QUEUE_STATUS_HOLD:
                                    listSkip.add(tempQueue);
                                    break;
                                case QUEUE_STATUS_GETIN:
                                    listGetin.add(tempQueue);
                                    break;
                            }
                        }
                    });
            loadQueueListener.onLoadSuccess();
        } else {
            loadQueueListener.onLoadSuccess();
        }
    }

    public ArrayList<RealmQueue> getListQueueByQueueState(QueueStatus state) {
        switch (state) {
            case QUEUE_STATUS_QUEUE:
                return listQueue;
            case QUEUE_STATUS_HOLD:
                return listSkip;
            case QUEUE_STATUS_GETIN:
                return listGetin;
        }
        return null;
    }

    public ArrayList<RealmQueue> getListQueue() {
        return this.listQueue;
    }

    public ArrayList<RealmQueue> getListSkip() {
        return this.listSkip;
    }

    public ArrayList<RealmQueue> getListGetin() {
        return this.listGetin;
    }

    public QueueStatus getState(int state) {
        switch (state) {
            case 0:
                return QUEUE_STATUS_QUEUE;
            case 1:
                return QUEUE_STATUS_HOLD;
            case 2:
                return QUEUE_STATUS_GETIN;
        }
        return null;
    }

    public void changeStateQueue(RealmQueue queueSelect, QueueStatus toStatus) {

        boolean isCancel = queueSelect.getQueueAction().equalsIgnoreCase(QueueAction.CANCEL.getValue()) || queueSelect.getQueueAction().equalsIgnoreCase(QueueAction.CANCELLED.getValue());
        ArrayList<RealmQueue> queueTempList = null;
        /**
         * Get Queue List Before Change
         */
        switch (QueueStatus.type(queueSelect.getQueueState())) {
            case QUEUE_STATUS_QUEUE:
                queueTempList = getListQueue();
                break;
            case QUEUE_STATUS_HOLD:
                queueTempList = getListSkip();
                break;
            case QUEUE_STATUS_GETIN:
                queueTempList = getListGetin();
                break;
        }

        /**
         * Remove from previous List
         */
        ListIterator<RealmQueue> listIterator = queueTempList.listIterator();
        while (listIterator.hasNext()) {
            RealmQueue q = listIterator.next();
            if (q.getQueueId().equalsIgnoreCase(queueSelect.getQueueId())) {
                Log.e("Queue Manager", "List Before Change " + q.getQueueState());
                listIterator.remove();
                break;
            }
        }


        /**
         * Update Queue Remain On Memory
         */

        if (!queueSelect.getQueueState().equalsIgnoreCase(QueueState.HOLD.getValue())) {
            for (int i = 0; i < queueTempList.size(); i++) {
                if (queueTempList.get(i).getQueueRemain() != (i + 1)) {
                    queueTempList.get(i).setQueueRemain(i + 1);
                    if (queueTempList.get(i).getQueueType().equalsIgnoreCase(QueueType.MOBILE.getValue())) {
                        addToStackQueue(queueTempList.get(i));
                    }
                }
            }
        } else {
        }

        /**
         * Update Queue Remain On DB
         */
        updateQueueRemain(queueTempList);


        /**
         * Change Status
         */
        queueSelect.setQueueState(toStatus.getValue());

        /**
         * Get Queue List After Change
         */
        if (hashmapQueue.containsKey(queueSelect.getQueueId()) && hashmapQueue.get(queueSelect.getQueueId()) != null) {
            Log.e("Queue Manager", "List After Change " + hashmapQueue.get(queueSelect.getQueueId()).getQueueState());
            switch (toStatus) {
                case QUEUE_STATUS_QUEUE:
                    queueTempList = getListQueue();
                    queueTempList.add(queueSelect);
                    Collections.sort(queueTempList, new Comparator<RealmQueue>() {
                        @Override
                        public int compare(RealmQueue realmQueue, RealmQueue t1) {
                            return Integer.valueOf(realmQueue.getQueueIndex()).compareTo(Integer.valueOf(t1.getQueueIndex()));
                        }
                    });
                    break;
                case QUEUE_STATUS_HOLD:
                    queueTempList = getListSkip();
                    queueTempList.add(queueSelect);
                    break;
                case QUEUE_STATUS_GETIN:
                    queueTempList = getListGetin();
                    queueTempList.add(queueSelect);
                    break;
            }

            if (!toStatus.getValue().equalsIgnoreCase(QueueState.HOLD.getValue())) {
                /**
                 * Update Queue Remain On Memory
                 * GET IN,Queue.
                 */
                if (toStatus.getValue().equalsIgnoreCase(QueueState.QUEUE.getValue())) {
                    /**
                     * Change to Queue
                     */
                    for (int i = 0; i < queueTempList.size(); i++) {
                        if (queueTempList.get(i).getQueueRemain() != (i + 1)) {
                            queueTempList.get(i).setQueueRemain(i + 1);
                            if (queueTempList.get(i).getQueueType().equalsIgnoreCase(QueueType.MOBILE.getValue())) {
                                addToStackQueue(queueTempList.get(i));
                            }
                        }
                    }
                } else {
                    /**
                     * Get in
                     */
                    queueSelect.setQueueAction(QueueAction.CLAIM.getValue());
                    queueSelect.setQueueRemain(0);
                    if (queueSelect.getQueueType().equalsIgnoreCase(QueueType.MOBILE.getValue())) {
                        addToStackQueue(queueSelect);
                        DDPManager.getInstance().claimQueue(queueSelect);
                    }
                }
                /**
                 * Update Queue Remain On DB
                 */
                updateQueueRemain(queueTempList);
            } else {
                updateQueueDetail(queueSelect);
                if (queueSelect.getQueueType().equalsIgnoreCase(QueueType.MOBILE.getValue()) && !isCancel) {
                    addToStackQueue(queueSelect);
                    DDPManager.getInstance().holdQueue(queueSelect);
                }
            }

        }

        RxBus.getInstance().send(new UpdateQueueChangeEvent());
        requestUpdateNoQueueOnTabMenu();
        if (DDPManager.getInstance() != null)
            DDPManager.getInstance().updateStackToServer();
    }

    private void requestUpdateNoQueueOnTabMenu() {
        RxBus.getInstance().send(new UpdateQueueCountTabMenuEvent());
    }

    public RealmQueue checkQueueAvailable(String queueId) {
        if (hashmapQueue.containsKey(queueId)) {
            return hashmapQueue.get(queueId);
        } else {
            RealmQueue result = Realm.getDefaultInstance().where(RealmQueue.class).equalTo("_id", queueId).findFirst();
            return result != null ? Realm.getDefaultInstance().copyFromRealm(result) : null;
        }
    }

    public RealmQueue checkQueueAvailableByRefCode(String refCode) {

        for (Map.Entry<String, RealmQueue> entry : hashmapQueue.entrySet()) {
            if (TextUtils.isEmpty(entry.getValue().getRefCode()) && entry.getValue().getRefCode().equals(refCode)) {
                return entry.getValue();
            }
        }
        RealmQueue result = Realm.getDefaultInstance().where(RealmQueue.class).equalTo("refCode", refCode).findFirst();
        return result != null ? Realm.getDefaultInstance().copyFromRealm(result) : null;
    }

    public void updateQueueDetail(RealmQueue queue) {
        Realm.getDefaultInstance().beginTransaction();
        Realm.getDefaultInstance().copyToRealmOrUpdate(queue);
        Realm.getDefaultInstance().commitTransaction();
        Log.e("Queue Manager", String.format("DB Update Queue Remain %s : %s%s", queue.getQueueId(), queue.getQueueGroup(), queue.getQueueIndex()));
    }

    public void updateQueueAction(RealmQueue queue) {
        Realm.getDefaultInstance().beginTransaction();
        Realm.getDefaultInstance().copyToRealmOrUpdate(queue);
        Realm.getDefaultInstance().commitTransaction();
        Log.e("Queue Manager", String.format("DB Update Queue Action %s%s : %s", queue.getQueueGroup(), queue.getQueueIndex(), queue.getQueueAction()));
    }

    private void updateQueueRemain(ArrayList<RealmQueue> listQueue) {
        Realm.getDefaultInstance().beginTransaction();
        Realm.getDefaultInstance().copyToRealmOrUpdate(listQueue);
        Realm.getDefaultInstance().commitTransaction();
        Log.e("Queue Manager", "DB Update Queue Remain " + listQueue.size() + " item");
    }

    private void addToStackQueue(RealmQueue queue) {
        if (queue.getQueueType().equalsIgnoreCase(QueueType.MOBILE.getValue()))
            DDPManager.getInstance().stackQueueForUpdate(queue);
    }


    public int getSumQueueInDB() {
        return Realm.getDefaultInstance().where(RealmQueue.class).findAll().size();
    }

    public RealmGroup getGroupAvailable() {
        return groupAvailable;
    }

    public String getGroupNameByCapacity() {
        return groupAvailable.getGroupName();
    }

    public int getQueueIndex() {
        return counterQueue += 1;
    }

    public int getCurrentQueueNumber() {
        return counterQueue;
    }

    public void addQueue(@NonNull String groupKey, @NonNull RealmQueue queue, AddQueueListener listener) {
        RealmResults<RealmQueue> result = Realm.getDefaultInstance().where(RealmQueue.class).equalTo("isReset", false).equalTo("_id", queue.getQueueId()).findAll();
        if (result.size() != 0) {
            listener.onFail("Already Queue");
        } else {
            addQueueToDB(queue, listener);
        }
    }

    public void deleteQueueInRealm(final RealmQueue queue) {
        Realm.getDefaultInstance().executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<RealmQueue> result = realm.where(RealmQueue.class).equalTo("_id", queue.getQueueId()).findAll();
                result.deleteAllFromRealm();
            }
        });
    }

    private void addQueueToDB(@NonNull final RealmQueue queue, final AddQueueListener listener) {
        transaction = Realm.getDefaultInstance().executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealm(queue);
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                hashmapQueue.put(queue.getQueueId(), queue);
                getListQueue().add(hashmapQueue.get(queue.getQueueId()));
                requestUpdateNoQueueOnTabMenu();
                listener.onAddSuccess();
            }
        });
    }

    public void terminateQueue(RealmQueue queue) {
        if (queue != null) {
            String queueId = queue.getQueueId();
            deleteQueueInRealm(queue);
            ArrayList<RealmQueue> queueTempList = null;
            switch (QueueStatus.type(queue.getQueueState())) {
                case QUEUE_STATUS_QUEUE:
                    queueTempList = getListQueue();
                    break;
                case QUEUE_STATUS_HOLD:
                    queueTempList = getListSkip();
                    break;
                case QUEUE_STATUS_GETIN:
                    queueTempList = getListGetin();
                    break;
            }
            if (queueTempList != null && queueTempList.size() != 0) {
                ListIterator<RealmQueue> listIterator = queueTempList.listIterator();
                while (listIterator.hasNext()) {
                    RealmQueue q = listIterator.next();
                    if (q.getQueueId().equalsIgnoreCase(queue.getQueueId())) {
                        Log.e("Queue Manager", "List Before Change " + q.getQueueState());
                        listIterator.remove();
                        break;
                    }
                }
            }
            hashmapQueue.remove(queueId);
        }
    }

    public void deleteInactiveQueue(RealmQueue queue) {
        ArrayList<RealmQueue> queueTempList = null;
        switch (QueueStatus.type(queue.getQueueState())) {
            case QUEUE_STATUS_QUEUE:
                queueTempList = getListQueue();
                break;
            case QUEUE_STATUS_HOLD:
                queueTempList = getListSkip();
                break;
            case QUEUE_STATUS_GETIN:
                queueTempList = getListGetin();
                break;
        }

        if (queueTempList != null) {
            queueTempList.remove(queue);
        }

        queue.setIsActive(false);
        queue.setQueueState(QueueAction.DELETED.getValue());
        queue.setQueueAction(QueueAction.DELETED.getValue());
        queue.setQueueRemain(0);
        updateQueueDetail(queue);


        if (!queue.getQueueState().equalsIgnoreCase(QueueState.HOLD.getValue())) {
            /**
             * Update Queue Remain On Memory
             */
            if (queue.getQueueState().equalsIgnoreCase(QueueState.QUEUE.getValue())) {
                for (int i = 0; i < (queueTempList != null ? queueTempList.size() : 0); i++) {
                    if (queueTempList.get(i).getQueueRemain() != (i + 1)) {
                        queueTempList.get(i).setQueueRemain(i + 1);
                        if (queueTempList.get(i).getQueueType().equalsIgnoreCase(QueueType.MOBILE.getValue())) {
                            addToStackQueue(queueTempList.get(i));
                        }
                    }
                }
            } else {
                queue.setQueueRemain(0);
                if (queue.getQueueType().equalsIgnoreCase(QueueType.MOBILE.getValue()))
                    addToStackQueue(queue);
            }

            /**
             * Update Queue Remain On DB
             */
            updateQueueRemain(queueTempList);
        } else {
            updateQueueDetail(queue);
            if (queue.getQueueType().equalsIgnoreCase(QueueType.MOBILE.getValue()))
                addToStackQueue(queue);
        }
        if (DDPManager.getInstance() != null)
            DDPManager.getInstance().updateStackToServer();

        hashmapQueue.remove(queue.getQueueId());
        RxBus.getInstance().send(new UpdateQueueChangeEvent());
        requestUpdateNoQueueOnTabMenu();
    }

    public void callQueue(RealmQueue queueSelect) {
        queueSelect.setQueueAction(QueueAction.CALL.getValue());
        Realm.getDefaultInstance().beginTransaction();
        Realm.getDefaultInstance().copyToRealmOrUpdate(queueSelect);
        Realm.getDefaultInstance().commitTransaction();
        if (queueSelect.getQueueType().equalsIgnoreCase(QueueType.MOBILE.getValue())) {
            DDPManager.getInstance().callQueue(queueSelect);
        }
    }

    public void resetQueueAutoTime(final ResetQueueListener listener) {
        /**
         * Reset by Time
         * set Queue Reset > true
         * clear queue in Display
         * upload queue data to server
         */
        Log.e("Reset", "resetQueueAutoTime");
        RealmResults<RealmQueue> results = Realm.getDefaultInstance()
                .where(RealmQueue.class)
                .equalTo("isReset", false)
                .notEqualTo("action", QueueAction.CLAIM.getValue())
                .notEqualTo("action", QueueAction.CANCELLED.getValue())
                .findAll();
        if (results.size() > 0) {
            Realm.getDefaultInstance().beginTransaction();
            StreamSupport.stream(results).forEach(new Consumer<RealmQueue>() {
                @Override
                public void accept(RealmQueue realmQueue) {
                    realmQueue.setQueueAction(QueueAction.DELETED.getValue());
                    realmQueue.setReset(true);
                    if (realmQueue.getQueueType().equalsIgnoreCase(QueueType.MOBILE.getValue()))
                        addToStackQueue(realmQueue);
                }
            });
            Realm.getDefaultInstance().commitTransaction();
            StreamSupport.stream(results).forEach(new Consumer<RealmQueue>() {
                @Override
                public void accept(RealmQueue realmQueue) {
                    updateQueueAction(realmQueue);
                }
            });
        }

        results = Realm.getDefaultInstance()
                .where(RealmQueue.class)
                .equalTo("isReset", false)
                .findAll();
        if (results.size() > 0) {
            Realm.getDefaultInstance().beginTransaction();
            StreamSupport.stream(results).forEach(new Consumer<RealmQueue>() {
                @Override
                public void accept(RealmQueue realmQueue) {
                    realmQueue.setReset(true);
                    if (realmQueue.getQueueType().equalsIgnoreCase(QueueType.MOBILE.getValue()))
                        addToStackQueue(realmQueue);
                }
            });
            Realm.getDefaultInstance().commitTransaction();
            StreamSupport.stream(results).forEach(new Consumer<RealmQueue>() {
                @Override
                public void accept(RealmQueue realmQueue) {
                    updateQueueAction(realmQueue);
                }
            });
        }

        if (DDPManager.getInstance() != null) {
            DDPManager.getInstance().updateStackToServerWithCallback(new DDPManager.UpdateStackToServerListener() {
                @Override
                public void onSuccess() {
                    Log.e("Reset", " onSuccess updateStackToServerWithCallback");
                    clearQueueInDisplay();
                    transaction = Realm.getDefaultInstance().executeTransactionAsync(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            realm.delete(RealmQueue.class);
                        }
                    });
                    ConfigApplication.prefs().setLastestSentReport(DateHelper.getMilliseconds());
                    listener.onSuccess();
                }

                @Override
                public void onError() {
                    Log.e("Reset", " onError updateStackToServerWithCallback");
                    clearQueueInDisplay();
                    transaction = Realm.getDefaultInstance().executeTransactionAsync(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            realm.delete(RealmQueue.class);
                        }
                    });
                    ConfigApplication.prefs().setLastestSentReport(DateHelper.getMilliseconds());
                    listener.onSuccess();
                }
            });
        } else {
            Log.e("Reset", " DDP not connect");
            clearQueueInDisplay();
            transaction = Realm.getDefaultInstance().executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.delete(RealmQueue.class);
                }
            });
            ConfigApplication.prefs().setLastestSentReport(DateHelper.getMilliseconds());
            listener.onSuccess();
        }
        listener.onError();
    }

    private void clearQueueInDisplay() {
        hashmapQueue.clear();
        listQueue.clear();
        listGetin.clear();
        listSkip.clear();
        counterQueue = 0;
        RxBus.getInstance().send(new UpdateQueueChangeEvent());
        RxBus.getInstance().send(new UpdateQueueCountTabMenuEvent());
    }

    public interface LoadQueueListener {
        void onLoadSuccess();
    }

    public interface AddQueueListener {
        void onAddSuccess();

        void onFail(String message);
    }

    public interface ResetQueueListener {
        void onSuccess();

        void onError();
    }

    public static class UpdateQueueCountTabMenuEvent {

    }

    public static class UpdateQueueChangeEvent {

    }

    public static class UpdateOpenService {
        private String serviceId;

        public UpdateOpenService(String serviceId) {
            this.serviceId = serviceId;
        }

        public String getServiceId() {
            return serviceId;
        }
    }
}
