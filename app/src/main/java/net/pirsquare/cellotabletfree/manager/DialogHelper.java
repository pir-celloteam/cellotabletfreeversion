package net.pirsquare.cellotabletfree.manager;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import net.pirsquare.cellotabletfree.R;

/**
 * Created by kung on 6/16/16.
 */
public class DialogHelper {

    private static MaterialDialog alertDialog;
    private static MaterialDialog loadingDialog;
    private static MaterialDialog progressDialog;

    public static void showProgressDialog(Activity mActivity, String title, String message) {
        progressDialog = new MaterialDialog.Builder(mActivity)
                .title(title)
                .content(message)
                .progress(true, 0)
                .progressIndeterminateStyle(false)
                .cancelable(false)
                .build();
        progressDialog.show();
    }

    public static void hideProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }


    public static void showLoadingDialog(Activity mActivity, String title, String content) {
        loadingDialog = new MaterialDialog.Builder(mActivity)
                .title(title)
                .content(content)
                .build();
        loadingDialog.show();
    }

    public static void hideLoadingDialog() {
        if (loadingDialog != null)
            loadingDialog.dismiss();
    }

    public static void openAlertDialog(Activity mActivity, String title, String detail, final DialogCallback callback) {
        if (alertDialog != null)
            alertDialog.dismiss();
        alertDialog = new MaterialDialog.Builder(mActivity)
                .title(title)
                .content(detail)
                .positiveText(mActivity.getString(R.string.text_accept))
                .positiveColorRes(R.color.grey)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                        if (callback != null)
                            callback.onClickOK();
                    }
                }).show();
    }

    public static void openAlertDialogForceCancel(Activity mActivity, String title, String detail, final DialogCallback callback) {
        if (alertDialog != null)
            alertDialog.dismiss();
        alertDialog = new MaterialDialog.Builder(mActivity)
                .title(title)
                .content(detail)
                .cancelable(false)
                .positiveText(mActivity.getString(R.string.text_accept))
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                        if (callback != null)
                            callback.onClickOK();
                    }
                }).show();
    }

    public static void openAlertDialogTwoWay(Activity mActivity, String title, String detail, final DialogCallback callback) {
        if (alertDialog != null)
            alertDialog.dismiss();
        alertDialog = new MaterialDialog.Builder(mActivity)
                .title(title)
                .content(detail)
                .positiveText(mActivity.getString(R.string.text_accept))
                .positiveColorRes(R.color.grey)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                        if (callback != null)
                            callback.onClickOK();
                    }
                })
                .negativeText(mActivity.getString(R.string.text_cancel))
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                        if (callback != null)
                            callback.onClickCancel();
                    }
                })
                .show();
    }

    public static void openLimitQueueDialog(final Activity activity) {
        if (alertDialog != null)
            alertDialog.dismiss();
        alertDialog = new MaterialDialog.Builder(activity)
                .title(activity.getString(R.string.app_name))
                .customView(R.layout.custom_alert_dialog, false)
                .positiveText("ซื้อ")
                .positiveColorRes(R.color.red)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://cello.biz/"));
                        activity.startActivity(browserIntent);
                    }
                })
                .negativeText("ไม่ใช่ตอนนี้")
                .negativeColorRes(R.color.grey)
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .show();
        TextView textDetail = (TextView) alertDialog.getCustomView().findViewById(R.id.text_detail);
        textDetail.setText("คิวของคุณหมดแล้ว\nต้องการซื้อจำนวนคิวเพิ่มหรือไม่?");
    }

    public static void openDialogUsePrinter(final Activity activity) {
        if (alertDialog != null)
            alertDialog.dismiss();
        alertDialog = new MaterialDialog.Builder(activity)
                .customView(R.layout.custom_alert_dialog, false)
                .title(activity.getString(R.string.app_name))
                .positiveText("สนใจ")
                .positiveColorRes(R.color.red)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://cello.biz/"));
                        activity.startActivity(browserIntent);
                    }
                })
                .negativeText("ไม่ใช่ตอนนี้")
                .negativeColorRes(R.color.grey)
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .show();
        TextView textDetail = (TextView) alertDialog.getCustomView().findViewById(R.id.text_detail);
        textDetail.setText("สามารถเชื่อมต่อ Printer เพื่อพิมพ์บัตรคิวได้?");
    }


    public interface DialogCallback {
        void onClickOK();

        void onClickCancel();
    }
}
