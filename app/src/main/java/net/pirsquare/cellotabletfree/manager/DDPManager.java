package net.pirsquare.cellotabletfree.manager;

import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

import net.pirsquare.cellotabletfree.login.LoginActivity;
import net.pirsquare.cellotabletfree.main.Constant;
import net.pirsquare.cellotabletfree.main.MainActivity;
import net.pirsquare.cellotabletfree.model.QueueAction;
import net.pirsquare.cellotabletfree.model.QueueStatus;
import net.pirsquare.cellotabletfree.model.QueueType;
import net.pirsquare.cellotabletfree.model.RealmQueue;
import net.pirsquare.cellotabletfree.model.RealmUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import im.delight.android.ddp.Meteor;
import im.delight.android.ddp.MeteorCallback;
import im.delight.android.ddp.ResultListener;
import im.delight.android.ddp.SubscribeListener;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subjects.BehaviorSubject;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by kampolleelaporn on 3/28/16.
 */
public class DDPManager implements MeteorCallback {

    private MainActivity mainActivity;
    private static DDPManager mInstance;
    private static Meteor mMeteor;
    private static String TAG = "METERO";
    private String subscriptionId;


    private BehaviorSubject<Boolean> observeStatusMeteor;


    private ArrayList<Map<String, Object>> listStackQueue;
    private HashMap<String, Object> queueHashmap;

    private static ConnectServerListener listener;

    private CompositeSubscription observable;
    private boolean isConnect;
    private ReconnectListener reconnectListener;
    private boolean isClose;

    public DDPManager() {
    }

    public void init(MainActivity context, ConnectServerListener mListener) {
        this.mainActivity = context;
        listener = mListener;
        Meteor.setLoggingEnabled(true);
        mMeteor = new Meteor(context, Constant.METEOR_SERVER_URL);
        mMeteor.addCallback(mInstance);
        mMeteor.connect();
    }

    public static DDPManager getInstance() {
        if (mInstance == null)
            mInstance = new DDPManager();
        return mInstance;
    }

    public static Meteor getMeteor() {
        return mMeteor;
    }

    public BehaviorSubject<Boolean> subscribeStatus() {
        BehaviorSubject<Boolean> observe = BehaviorSubject.create(mMeteor.isConnected());
        observe.subscribeOn(Schedulers.io())
                .distinctUntilChanged()
                .observeOn(AndroidSchedulers.mainThread());
        observeStatusMeteor = observe;
        return observe;
    }

    public void postStatusMeteor(boolean status) {
        if (observeStatusMeteor != null)
            observeStatusMeteor.onNext(status);
    }


    public boolean isConnect() {
        return isConnect;
    }


    @Override
    public void onConnect(boolean signedInAutomatically) {
        isConnect = true;
        Log.e(TAG, "Connected");
        Log.e(TAG, "Is logged in: " + mMeteor.isLoggedIn());
        Log.e(TAG, "User ID: " + mMeteor.getUserId());
        if (signedInAutomatically) {
            if (observable != null) {
                observable.unsubscribe();
            }
        }
        if (!mMeteor.isLoggedIn() && TabletConfig.pref().isLogin()) {
            Log.e("Login", String.format("%s : %s", TabletConfig.pref().getUser(), TabletConfig.pref().getSerialNumber()));
            mMeteor.loginWithUsername(TabletConfig.pref().getUser(), TabletConfig.pref().getSerialNumber(), new ResultListener() {
                @Override
                public void onSuccess(String s) {
                    Log.e(TAG, s);
                    subscribe();
                }

                @Override
                public void onError(String s, String s1, String s2) {
                    Log.e(TAG, s + " : " + s1 + " : " + s2);
                    if (listener != null)
                        listener.onFail();
                    postStatusMeteor(false);
                }
            });
        } else {
            subscribe();
        }

        if (reconnectListener != null) {
            reconnectListener.reconnectSuccess();
        }
    }


    private void subscribe() {
        Map<String, Object> condition = new HashMap<>();
        condition.put("$nin", Arrays.asList(QueueAction.CANCELLED.getValue(), QueueAction.CLAIM.getValue(), QueueAction.DELETED.getValue()));
        Map<String, Object> conditionDate = new HashMap<>();
        conditionDate.put("$gte", DateHelper.getMidnight());
        Map<String, Object> param = new HashMap<>();
        param.put("status", condition);
        param.put("createdAt", conditionDate);

        param.put("branch.id", TabletConfig.pref().getBranchId());

        subscriptionId = mMeteor.subscribe("queues.list", new Object[]{param}, new SubscribeListener() {
            @Override
            public void onSuccess() {
                Log.e(TAG, "Subscript Success");
                updateStackToServer();
                if (listener != null)
                    listener.onSuccess();
                postStatusMeteor(true);
                if (DDPManager.getInstance().isConnect()) {
                    DDPManager.getInstance().setAllowMobileReserve(TabletConfig.pref().getQueueCurrentQueueReserve() < TabletConfig.pref().getQueueMaxAvailableReserve(), new DDPManager.AllowMobileReserveListener() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onFail() {

                        }
                    });
                }

            }

            @Override
            public void onError(String s, String s1, String s2) {
                Log.e(TAG, "Subscript Fail" + s + " : " + s1 + " : " + s2);
                if (listener != null)
                    listener.onFail();
                postStatusMeteor(false);
            }
        });
    }

    public void onReconnect(ReconnectListener listener) {
        if (TabletConfig.pref().isLogin()) {
            mMeteor.reconnect();
            this.reconnectListener = listener;
        }
    }

    @Override
    public void onDisconnect() {
        Log.e(TAG, "Server Disconnect");
        isConnect = false;
        postStatusMeteor(false);
        if (!isClose) {
            if (listener != null)
                listener.onFail();
            isConnect = false;
            if (reconnectListener != null) {
                mMeteor.disconnect();
                reconnectListener.reconnectFail();
                reconnectListener = null;
            }
        }
    }

    @Override
    public void onException(Exception e) {
        Log.e(TAG, "Exception");
        mMeteor.disconnect();
        if (e != null) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDataAdded(String s, String s1, String s2) {
        Log.e(TAG, " added to <" + s + "> in <" + s1 + ">");
        Log.e(TAG, "    Added: " + s2);
        if (!s.equalsIgnoreCase("users")) {
            JSONObject json = null;
            int capacity = 0;
            try {
                json = new JSONObject(s2);
                capacity = Integer.valueOf(json.getString("capacity"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (json != null) {
                RealmQueue oldQueue = QueueManager.getInstance().checkQueueAvailable(s1);
                if (oldQueue == null) {
                    try {
                        String serviceId = "";
                        String serviceName = "";
                        String id = "";
                        String userName = "";
                        String telephone = "";
                        String locale = "";

                        if (json.has("status") && json.getString("status").equalsIgnoreCase("convert") && json.has("refCode")) {

                            /**
                             * Covert queue by refCode
                             * find queue by refCode
                             */

                            RealmQueue queue = QueueManager.getInstance().checkQueueAvailableByRefCode(json.getString("refCode"));
                            if (queue != null && !queue.isReset()) {
                                if (json.has("user")) {
                                    try {
                                        if (json.getJSONObject("user").has("id"))
                                            id = json.getJSONObject("user").getString("id");
                                        if (json.getJSONObject("user").has("display_name"))
                                            userName = json.getJSONObject("user").getString("display_name");
                                        if (json.getJSONObject("user").has("phone_number"))
                                            telephone = json.getJSONObject("user").getString("phone_number");
                                        if (json.getJSONObject("user").has("locale"))
                                            locale = json.getJSONObject("user").getString("locale");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    RealmUser user = new RealmUser();
                                    user.setId(id);
                                    user.setName(userName);
                                    user.setTelephone(telephone);
                                    user.setLocal(locale);
                                    queue.setUser(user);
                                }
                                addQueueFromOldQueue(s1
                                        , queue.getQueueGroup()
                                        , queue.getQueueIndex()
                                        , queue.getOrderNumber()
                                        , queue.getQueueAction()
                                        , queue.getQueueState()
                                        , queue.getQueueRemain()
                                        , queue.getTimeReserve()
                                        , queue.getUser() != null ? queue.getUser().getId() : null
                                        , queue.getUser() != null ? queue.getUser().getName() : null
                                        , queue.getUser() != null ? queue.getUser().getTelephone() : null
                                        , queue.getUser() != null ? queue.getUser().getLocal() : "en"
                                        , queue.getNumSeat()
                                        , queue.getServiceId()
                                        , queue.getServiceName());
                                QueueManager.getInstance().terminateQueue(queue);

                            }

                        } else if (json.has("status") && json.getString("status").equalsIgnoreCase("convert")) {
                            /**
                             * Incorrect Queue
                             * Cancel now
                             */
                            stackQueueForDelete(s1, QueueAction.CANCELLED.getValue(), 0);
                            updateStackToServer();
                        } else {
                            /**
                             * Reserve Queue
                             * Create new queue
                             */

                            if (json.has("service")) {
                                if (json.getJSONObject("service").has("id"))
                                    serviceId = json.getJSONObject("service").getString("id");
                                if (json.getJSONObject("service").has("name"))
                                    serviceName = json.getJSONObject("service").getString("name");
                            }
                            if (json.has("user")) {
                                if (json.getJSONObject("user").has("id"))
                                    id = json.getJSONObject("user").getString("id");
                                if (json.getJSONObject("user").has("display_name"))
                                    userName = json.getJSONObject("user").getString("display_name");
                                if (json.getJSONObject("user").has("phone_number"))
                                    telephone = json.getJSONObject("user").getString("phone_number");
                                if (json.getJSONObject("user").has("locale"))
                                    locale = json.getJSONObject("user").getString("locale");
                            }
                            if (!TextUtils.isEmpty(serviceId))
                                addQueue(s1, id, userName, telephone.equalsIgnoreCase("null") ? null : telephone, TextUtils.isEmpty(locale) ? "en" : locale, capacity, serviceId, serviceName);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else if (oldQueue != null && oldQueue.getQueueAction().equalsIgnoreCase(QueueAction.RESERVE.getValue())) {
                    /**
                     * Accepted Queue For Already queue in DB but not Accepted
                     */

                    oldQueue.setQueueAction(QueueAction.ACCEPT.getValue());
                    stackQueueForUpdate(oldQueue);

                } else if (oldQueue != null && oldQueue.getQueueAction().equalsIgnoreCase(QueueAction.CANCELLED.getValue())) {
                    /**
                     * Update Mobile queue in Server for cancel when reset queue
                     */
                    oldQueue.setQueueAction(QueueAction.CANCELLED.getValue());
                    oldQueue.setIsActive(false);
                    QueueManager.getInstance().updateQueueAction(oldQueue);
                    stackQueueForDelete(oldQueue.getQueueId(), QueueAction.CANCELLED.getValue(), 0);
                    updateStackToServer();
                } else if (oldQueue != null && oldQueue.getQueueType().equalsIgnoreCase(QueueType.PAPER.getValue())) {
                    /**
                     * Convert Queue
                     */
                    if (!oldQueue.isReset()) {
                        String id = "";
                        String userName = "";
                        String telephone = "";
                        String locale = "";
                        if (json.has("user")) {
                            try {
                                if (json.getJSONObject("user").has("id"))
                                    id = json.getJSONObject("user").getString("id");
                                if (json.getJSONObject("user").has("display_name"))
                                    userName = json.getJSONObject("user").getString("display_name");
                                if (json.getJSONObject("user").has("phone_number"))
                                    telephone = json.getJSONObject("user").getString("phone_number");
                                if (json.getJSONObject("user").has("locale"))
                                    locale = json.getJSONObject("user").getString("locale");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            RealmUser user = new RealmUser();
                            user.setId(id);
                            user.setName(userName);
                            user.setTelephone(telephone);
                            user.setLocal(locale);
                            oldQueue.setUser(user);
                        }
                        oldQueue.setQueueAction(QueueAction.ACCEPT.getValue());
                        oldQueue.setQueueType(QueueType.MOBILE.getValue());
                        acceptConvertQueue(oldQueue);
                    } else {

                        /**
                         * Old queue is Reset
                         */
                        oldQueue.setQueueAction(QueueAction.CANCELLED.getValue());
                        oldQueue.setQueueRemain(0);
                        oldQueue.setQueueType(QueueType.MOBILE.getValue());
                        acceptConvertQueue(oldQueue);
                    }

                } else if (oldQueue != null) {
                    /**
                     * Old Queue in DB
                     */
                    try {
                        if (json.getString("status").equalsIgnoreCase("cancel")) {
                            /**
                             * User (Mobile) Cancel Queue
                             * Update Queue in Tablet and Confirm cancelled to Server
                             */
                            oldQueue.setQueueAction(QueueAction.CANCELLED.getValue());
                            oldQueue.setIsActive(true);
                            oldQueue.setCancel(true);
                            stackQueueForUpdate(oldQueue);
                            updateStackToServer();
                            RxBus.getInstance().send(new QueueManager.UpdateQueueChangeEvent());
                            QueueManager.getInstance().updateQueueDetail(oldQueue);
                            RxBus.getInstance().send(new DeleteQueueEvent(oldQueue.getQueueId()));
                            QueueManager.getInstance().changeStateQueue(oldQueue, QueueStatus.QUEUE_STATUS_HOLD);
                        } else if (oldQueue.getQueueRemain() != json.getInt("remain")) {
                            /**
                             * Queue Remain in Server Not Update
                             * Update Queue Remain in Server
                             */
                            stackQueueForUpdate(oldQueue);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    @Override
    public void onDataChanged(String s, String s1, String s2, String s3) {
        Log.e(TAG, "  changed in <" + s + "> in <" + s1 + ">");
        Log.e(TAG, "    Updated: " + s2);
        Log.e(TAG, "    Removed: " + s3);

        try {
            JSONObject json = new JSONObject(s2);
            if (json.has("status") && json.getString("status").equalsIgnoreCase(QueueAction.CANCEL.getValue())) {
                RealmQueue oldQueue = QueueManager.getInstance().checkQueueAvailable(s1);
                if (oldQueue != null) {
                    RxBus.getInstance().send(new MobileDeleteQueueEvent(oldQueue.getQueueId()));
                    oldQueue.setQueueAction(QueueAction.CANCELLED.getValue());
                    oldQueue.setIsActive(true);
                    oldQueue.setCancel(true);
                    stackQueueForUpdate(oldQueue);
                    updateStackToServer();
                    RxBus.getInstance().send(new QueueManager.UpdateQueueChangeEvent());
                    QueueManager.getInstance().updateQueueDetail(oldQueue);
                    RxBus.getInstance().send(new DeleteQueueEvent(oldQueue.getQueueId()));
                    QueueManager.getInstance().changeStateQueue(oldQueue, QueueStatus.QUEUE_STATUS_HOLD);

                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (s.equalsIgnoreCase("users")) {
            listener = null;
            isClose = true;
            try {
                JSONObject json = new JSONObject(s2);
                if (json.has("profile") && json.getJSONObject("profile").has("serialNumber")) {
                    /**
                     * Account Change Goto Setting
                     */
                    mMeteor.logout();
                    mMeteor.disconnect();
                    TabletConfig.pref().setIdTablet(null);
                    TabletConfig.pref().setIsLogin(false);
                    TabletConfig.pref().setUser(null);
                    TabletConfig.pref().setSerialNumber(null);
                    TabletConfig.pref().setPrinterMacAddress(null);
                    TabletConfig.pref().setTvMacAddress(null);
                    TabletConfig.pref().setTabletMacAddress(null);
                    TabletConfig.pref().setBranchId(null);
                    TabletConfig.pref().setLogo(null);
                    TabletConfig.pref().setHoldExpire(0);
                    TabletConfig.pref().setBranchName(null);
                    TabletConfig.pref().setS3BucketName(null);
                    TabletConfig.pref().setS3KeyDirectory(null);

                    TabletConfig.pref().setPrinterTopImagePath(null);
                    TabletConfig.pref().setPrinterBottomImagePath(null);

                    Intent intent = new Intent(mainActivity, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    mainActivity.startActivity(intent);
                    mainActivity.finish();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void onDataRemoved(String s, String s1) {
        Log.e(TAG, "Queue removed from <" + s + "> in document <" + s1 + ">");
    }


    public void stackQueueForUpdate(RealmQueue queue) {
        if (listStackQueue == null) {
            listStackQueue = new ArrayList<>();
        }
        queueHashmap = new HashMap<>();
        queueHashmap.put("_id", queue.getQueueId());
        queueHashmap.put("status", queue.getQueueAction());
        queueHashmap.put("remain", queue.getQueueRemain());
        listStackQueue.add(queueHashmap);
    }

    public void stackQueueForDelete(String queueId, String status, int remain) {
        if (listStackQueue == null) {
            listStackQueue = new ArrayList<>();
        }
        queueHashmap = new HashMap<>();
        queueHashmap.put("_id", queueId);
        queueHashmap.put("status", status);
        queueHashmap.put("remain", remain);
        listStackQueue.add(queueHashmap);
    }

    public void updateStackToServer() {
        if (listStackQueue != null && listStackQueue.size() != 0) {
            Log.e("Queue", listStackQueue.toString());
            mMeteor.call("queues.update", new Object[]{listStackQueue}, new ResultListener() {
                @Override
                public void onSuccess(String s) {
                    Log.e(TAG, "Stack Update Success " + s);
                    listStackQueue.clear();
                }

                @Override
                public void onError(String s, String s1, String s2) {
                    Log.e(TAG, "Stack Update Error " + s + " : " + s1);

                }
            });
        }
    }

    public void updateStackToServerWithCallback(final UpdateStackToServerListener listener) {
        if (listStackQueue != null && listStackQueue.size() != 0) {
            if (!mMeteor.isConnected())
                listener.onError();
            mMeteor.call("queues.update", new Object[]{listStackQueue}, new ResultListener() {
                @Override
                public void onSuccess(String s) {
                    Log.e(TAG, "Stack Update Success " + s);
                    listStackQueue.clear();
                    listener.onSuccess();
                }

                @Override
                public void onError(String s, String s1, String s2) {
                    Log.e(TAG, "Stack Update Error " + s + " : " + s1);
                    listener.onError();
                }
            });
        } else {
            listener.onError();
        }
    }

    public interface UpdateStackToServerListener {
        void onSuccess();

        void onError();
    }


    private void addQueue(final String queueId, String id, String userName, String telephone, String locale, int capacity, String serviceId, String serviceName) {
        Log.e("Accept Queue Mobile", "Add Queue Mobile " + queueId);
        final RealmQueue queue = new RealmQueue();
        queue.setQueueId(queueId);
        queue.setOrderNumber(String.valueOf(QueueManager.getInstance().getSumQueueInDB()));
        queue.setRefCode(EncodeHelper.getmInstance().getRef(queue.getOrderNumber(), DateHelper.getCurrentDateTime().getDayOfMonth(), TabletConfig.pref().getBranchIndex()));
        RealmUser user = new RealmUser();
        user.setId(TextUtils.isEmpty(id) ? null : id);
        user.setName(TextUtils.isEmpty(userName) ? "Guest" : userName);
        user.setLocal(locale);
        user.setTelephone(TextUtils.isEmpty(telephone) || telephone.equalsIgnoreCase("null") ? null : telephone);

        queue.setUser(user);
        queue.setQueueGroup(QueueManager.getInstance().getGroupNameByCapacity());
        queue.setQueueIndex(String.valueOf(QueueManager.getInstance().getQueueIndex()));
        queue.setIsActive(true);
        queue.setReset(false);
        queue.setQueueType(QueueType.MOBILE.getValue());
        queue.setQueueAction(QueueAction.ACCEPT.getValue());
        queue.setQueueState(QueueStatus.QUEUE_STATUS_QUEUE.getValue());
        queue.setNumSeat(capacity);
        queue.setBranchId(TabletConfig.pref().getBranchId());
        queue.setTimeReserve(DateHelper.getCurrent());
        queue.setQueueRemain(QueueManager.getInstance().getListQueue().size() + 1);
        queue.setRemainStart(queue.getQueueRemain());
        queue.setServiceId(TextUtils.isEmpty(serviceId) ? null : serviceId);
        queue.setServiceName(TextUtils.isEmpty(serviceName) ? null : serviceName);
        QueueManager.getInstance().addQueue(queue.getQueueGroup(), queue, new QueueManager.AddQueueListener() {
            @Override
            public void onAddSuccess() {
                Log.e("Queue", "Reserve Queue Success : " + queue.getQueueGroup() + queue.getQueueIndex());
                Collections.sort(QueueManager.getInstance().getListQueue(), new Comparator<RealmQueue>() {
                    @Override
                    public int compare(RealmQueue realmQueue, RealmQueue t1) {
                        return Integer.valueOf(realmQueue.getQueueIndex()).compareTo(Integer.valueOf(t1.getQueueIndex()));
                    }
                });
                Log.e("Accept Queue Mobile", "Update List Success " + queueId);
                RxBus.getInstance().send(new QueueManager.UpdateQueueChangeEvent());
                acceptQueue(queue);

                TabletConfig.pref().setQueueCurrentQueueReserve(TabletConfig.pref().getQueueCurrentQueueReserve() + 1);
                if (TabletConfig.pref().getQueueCurrentQueueReserve() == TabletConfig.pref().getQueueMaxAvailableReserve()) {
                    if (DDPManager.getInstance().isConnect()) {
                        DDPManager.getInstance().setAllowMobileReserve(false, new DDPManager.AllowMobileReserveListener() {
                            @Override
                            public void onSuccess() {
                                DialogHelper.openLimitQueueDialog(mainActivity);
                            }

                            @Override
                            public void onFail() {

                            }
                        });
                    }
                }
            }

            @Override
            public void onFail(String message) {
                Log.e(TAG, message);
            }
        });
    }

    private void acceptConvertQueue(final RealmQueue queue) {
        Map<String, Object> query = new HashMap<>();
        query.put("_id", queue.getQueueId());
        query.put("group", queue.getQueueGroup());
        query.put("index", queue.getQueueIndex());
        query.put("status", queue.getQueueAction());
        query.put("remain", queue.getQueueRemain());
        query.put("capacity", queue.getNumSeat());
        query.put("refCode", queue.getRefCode());
        if (!TextUtils.isEmpty(queue.getServiceId())) {
            Map<String, Object> service = new HashMap<>();
            service.put("id", queue.getServiceId());
            service.put("name", queue.getServiceName());
            query.put("service", service);
        }
        mMeteor.call("queues.accept", new Object[]{query}, new ResultListener() {
            @Override
            public void onSuccess(String s) {
                Log.e(TAG, "Accept Convert Queue Success " + s);
                QueueManager.getInstance().updateQueueDetail(queue);
                RxBus.getInstance().send(new QueueManager.UpdateQueueChangeEvent());
            }

            @Override
            public void onError(String s, String s1, String s2) {
                Log.e(TAG, "Accept Convert Queue Error " + s);
            }
        });
    }

    private void addQueueFromOldQueue(String queueId, String groupName, String queueIndex, String queueOrderNumber, String queueAction, String queueState, int queueRemain, String timeReserve, String id, String userName, String telephone, String locale, int capacity, String serviceId, String serviceName) {
        Log.e("Accept Queue Mobile", "Ole Queue Mobile " + queueId);
        final RealmQueue queue = new RealmQueue();
        queue.setQueueId(queueId);
        queue.setOrderNumber(queueOrderNumber);
        queue.setRefCode(EncodeHelper.getmInstance().getRef(queue.getOrderNumber(), DateHelper.getCurrentDateTime().getDayOfMonth(), TabletConfig.pref().getBranchIndex()));
        RealmUser user = new RealmUser();
        user.setId(TextUtils.isEmpty(id) ? null : id);
        user.setName(TextUtils.isEmpty(userName) ? "Guest" : userName);
        user.setLocal(locale);
        user.setTelephone(TextUtils.isEmpty(telephone) || telephone.equalsIgnoreCase("null") ? null : telephone);

        queue.setUser(user);
        queue.setQueueGroup(groupName);
        queue.setQueueIndex(queueIndex);
        queue.setIsActive(true);
        queue.setReset(false);
        queue.setQueueType(QueueType.MOBILE.getValue());
        queue.setQueueAction(queueAction);
        queue.setQueueState(queueState);
        queue.setNumSeat(capacity);
        queue.setBranchId(TabletConfig.pref().getBranchId());
        queue.setTimeReserve(timeReserve);
        queue.setQueueRemain(queueRemain);
        queue.setRemainStart(queue.getQueueRemain());
        queue.setServiceId(TextUtils.isEmpty(serviceId) ? null : serviceId);
        queue.setServiceName(TextUtils.isEmpty(serviceName) ? null : serviceName);
        QueueManager.getInstance().addQueue(queue.getQueueGroup(), queue, new QueueManager.AddQueueListener() {
            @Override
            public void onAddSuccess() {
                Collections.sort(QueueManager.getInstance().getListQueue(), new Comparator<RealmQueue>() {
                    @Override
                    public int compare(RealmQueue realmQueue, RealmQueue t1) {
                        return Integer.valueOf(realmQueue.getQueueIndex()).compareTo(Integer.valueOf(t1.getQueueIndex()));
                    }
                });
                RxBus.getInstance().send(new QueueManager.UpdateQueueChangeEvent());
                acceptQueue(queue);
            }

            @Override
            public void onFail(String message) {
                Log.e(TAG, message);
            }
        });
    }


    private void acceptQueue(RealmQueue queue) {
        /**
         * Accept Queue from Mobile Reserve
         */
        Map<String, Object> query = new HashMap<>();
        query.put("_id", queue.getQueueId());
        query.put("group", queue.getQueueGroup());
        query.put("index", queue.getQueueIndex());
        query.put("status", QueueAction.ACCEPT.getValue());
        query.put("remain", queue.getQueueRemain());
        query.put("capacity", queue.getNumSeat());
        query.put("refCode", queue.getRefCode());
        mMeteor.call("queues.accept", new Object[]{query}, new ResultListener() {
            @Override
            public void onSuccess(String s) {
                Log.e(TAG, "Accept Success " + s);
            }

            @Override
            public void onError(String s, String s1, String s2) {
                Log.e(TAG, "Accept Error " + s);
            }
        });

    }

    public void callQueue(RealmQueue queue) {
        Map<String, Object> data = new HashMap<>();
        data.put("_id", queue.getQueueId());
        if (!TextUtils.isEmpty(queue.getDestination()))
            data.put("destination", queue.getDestination());
        mMeteor.call("queues.call", new Object[]{data}, new ResultListener() {
            @Override
            public void onSuccess(String s) {
                Log.e(TAG, "Update Call to Server Success " + s);
            }

            @Override
            public void onError(String s, String s1, String s2) {
                Log.e(TAG, "Call " + s + " : " + s1 + " : " + s2);
            }
        });
    }

    public void claimQueue(RealmQueue queue) {
        Map<String, Object> data = new HashMap<>();
        data.put("_id", queue.getQueueId());
        mMeteor.call("queues.claim", new Object[]{data}, new ResultListener() {
            @Override
            public void onSuccess(String s) {
                Log.e(TAG, "Update Claim to Server Success " + s);
            }

            @Override
            public void onError(String s, String s1, String s2) {
                Log.e(TAG, "Claim " + s + " : " + s1 + " : " + s2);
            }
        });
    }

    public void holdQueue(RealmQueue queue) {
        Map<String, Object> data = new HashMap<>();
        data.put("_id", queue.getQueueId());
        mMeteor.call("queues.hold", new Object[]{data}, new ResultListener() {
            @Override
            public void onSuccess(String s) {
                Log.e(TAG, "Update Hold to Server Success " + s);
            }

            @Override
            public void onError(String s, String s1, String s2) {
                Log.e(TAG, "Hold " + s + " : " + s1 + " : " + s2);
            }
        });
    }

    public void expireQueue(RealmQueue queue) {
        Map<String, Object> data = new HashMap<>();
        data.put("_id", queue.getQueueId());
        mMeteor.call("queues.expire", new Object[]{data}, new ResultListener() {
            @Override
            public void onSuccess(String s) {
                Log.e(TAG, "Update Expired to Server Success " + s);
            }

            @Override
            public void onError(String s, String s1, String s2) {
                Log.e(TAG, "Expired " + s + " : " + s1 + " : " + s2);
            }
        });
    }

    public void close() {
        Log.e(TAG, "Disconect");
        isClose = true;
        if (mMeteor != null && mMeteor.isConnected())
            mMeteor.disconnect();
        if (observable != null)
            observable.unsubscribe();
    }

    public void logout() {
        if (mMeteor.isLoggedIn())
            mMeteor.logout(new ResultListener() {
                @Override
                public void onSuccess(String s) {
                    Log.e(TAG, "Log out Success");
                }

                @Override
                public void onError(String s, String s1, String s2) {
                    Log.e(TAG, "Log out Error " + s1);
                }
            });
    }

    public void setAllowMobileReserve(boolean isAllowMobileReserve, final AllowMobileReserveListener listener) {
        Map<String, Object> data = new HashMap<>();
        data.put("_id", TabletConfig.pref().getIdTablet());
        data.put("allowReserve", isAllowMobileReserve);
        mMeteor.call("tablets.allowReserve", new Object[]{data}, new ResultListener() {
            @Override
            public void onSuccess(String s) {
                Log.e(TAG, "setAllowMobileReserve Success " + s);
                listener.onSuccess();
            }

            @Override
            public void onError(String s, String s1, String s2) {
                Log.e(TAG, "setAllowMobileReserve " + s + " : " + s1 + " : " + s2);
                listener.onFail();
            }
        });
    }


    public interface ConnectServerListener {
        void onSuccess();

        void onFail();
    }

    public interface ReconnectListener {
        void reconnectSuccess();

        void reconnectFail();
    }

    public interface AllowMobileReserveListener {
        void onSuccess();

        void onFail();
    }


    public static class DeleteQueueEvent {
        private String queueId;

        public DeleteQueueEvent(String queueId) {
            this.queueId = queueId;
        }

        public String getQueueId() {
            return queueId;
        }
    }

    public static class MobileDeleteQueueEvent {
        private String queueId;

        public MobileDeleteQueueEvent(String queueId) {
            this.queueId = queueId;
        }

        public String getQueueId() {
            return queueId;
        }
    }

}