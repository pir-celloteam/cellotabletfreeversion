package net.pirsquare.sewoo;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.bixolon.printer.BixolonPrinter;
import com.sewoo.jpos.command.ESCPOSConst;
import com.sewoo.jpos.printer.ESCPOSPrinter;
import com.sewoo.jpos.printer.LKPrint;
import com.sewoo.port.android.BluetoothPort;
import com.sewoo.request.android.RequestHandler;

import java.io.IOException;
import java.util.Set;

public class Printer {
    public static int LANG = 0;
    public static final String TAG = "PRINTER";

    public static int DELAY_DIALOGUE = 500;

    private static final String MAC_SEWOO_PATTERN = "00:13:7B";
    private static final String MAC_BIXOLON_PATTERN = "74:F0:7D";

    public static final int TYPE_SEWOO = 1;
    public static final int TYPE_BIXOLON = 2;

    public static final int ALIGN_LEFT = 0;
    public static final int ALIGN_CENTER = 1;
    public static final int ALIGN_RIGHT = 2;

    public static final int TXT_1WIDTH = 0;
    public static final int TXT_2WIDTH = 16;
    public static final int TXT_3WIDTH = 32;
    public static final int TXT_4WIDTH = 48;
    public static final int TXT_5WIDTH = 64;
    public static final int TXT_6WIDTH = 80;
    public static final int TXT_7WIDTH = 96;
    public static final int TXT_8WIDTH = 112;

    public static final int TXT_1HEIGHT = 0;
    public static final int TXT_2HEIGHT = 1;
    public static final int TXT_3HEIGHT = 2;
    public static final int TXT_4HEIGHT = 3;
    public static final int TXT_5HEIGHT = 4;
    public static final int TXT_6HEIGHT = 5;
    public static final int TXT_7HEIGHT = 6;
    public static final int TXT_8HEIGHT = 7;

    public static final int MAX_WIDTH = 384;
    public static final int BITMAP_SIZE_NORMAL = 0;
    public static final int BITMAP_SIZE_DOUBLE = 3;

    private int current_type = 0;

    public int getCurrent_type() {
        return current_type;
    }

    public String current_address = "";

    private static Thread hThread;

    private static ESCPOSPrinter mSewooPrinter;
    private static BluetoothPort mSewooBluetoothPort;

    private static BixolonPrinter mBixolonPrinter;
    private static int mBixolonOnlineStatus = BixolonPrinter.STATE_NONE;

    private Context _context;
    private PrintManager.PrinterListener listener;

    public Printer(Context context, String forceAddress, PrintManager.PrinterListener listener) {
        this._context = context;
        this.listener = listener;

        if (forceAddress == null || forceAddress == "auto")
            getPairBluetoothAddress();
        else {
            current_address = forceAddress;

            Boolean isSewooPrinter = current_address.indexOf(MAC_SEWOO_PATTERN) == 0;

            Boolean isBIXOLONPrinter = current_address.indexOf(MAC_BIXOLON_PATTERN) == 0;

            if (isBIXOLONPrinter) {
                current_type = TYPE_BIXOLON;
            } else if (isSewooPrinter) {
                current_type = TYPE_SEWOO;
            }
        }
    }

    private static ProgressDialog dialog;// = new ProgressDialog(_context);

    public void preparePrint(final Runnable callback, final int align) {


        if (getCurrent_type() == TYPE_SEWOO) {
            mSewooBluetoothPort = BluetoothPort.getInstance();


            if (!mSewooBluetoothPort.isConnected()) {

                dialog = new ProgressDialog(_context);

                dialog.setCancelable(false);
                if (LANG == 0)
                    dialog.setMessage("Connecting");
                else
                    dialog.setMessage("กำลังเชื่อมต่อ");
                dialog.show();

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        // TODO Auto-generated method stub
                        // Log.i(_TAG, " new connTask().execute()");
                        // new connTask().execute(mSewooBluetoothPort, device,
                        // callback);
                        try {
                            final BluetoothDevice device = BluetoothAdapter
                                    .getDefaultAdapter().getRemoteDevice(
                                            current_address);

                            mSewooBluetoothPort.connect(device);

                            if ((hThread != null) && (hThread.isAlive())) {
                                hThread.interrupt();
                                hThread = null;
                            }

                            RequestHandler rh = new RequestHandler();
                            hThread = new Thread(rh);
                            hThread.start();

                            mSewooPrinter = new ESCPOSPrinter();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        listener.onConnect();
                        if (LANG == 0)
                            dialog.setMessage("Printing");
                        else
                            dialog.setMessage("กำลังพิมพ์");

                        printText(" ", align, TXT_1WIDTH, TXT_1HEIGHT, false);
                        pixelFeed(1);

                        callback.run();

                        if (dialog != null) {
                            if (dialog.isShowing())
                                dialog.dismiss();
                        }
                    }
                }, DELAY_DIALOGUE);

            } else {
                // check connection
                try {
                    mSewooPrinter.printerSts();
                } catch (Exception e) {
                    try {
                        mSewooBluetoothPort.disconnect();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    } catch (InterruptedException e1) {
                        e1.printStackTrace();
                    }
                    listener.onConnect();
                    if (LANG == 0)
                        dialog.setMessage("Printing");
                    else
                        dialog.setMessage("กำลังพิมพ์");

                    preparePrint(callback, align);

                    if (dialog != null) {
                        if (dialog.isShowing())
                            dialog.dismiss();
                    }

                    return;
                }

                callback.run();
            }

        } else if (getCurrent_type() == TYPE_BIXOLON) {

            // Log.i(_TAG, " type=" + current_type);

            if (mBixolonPrinter == null) {
                try {

                    // Log.i(_TAG, " new BixolonPrinter");

                    mBixolonPrinter = new BixolonPrinter(_context, new Handler(
                            new Handler.Callback() {
                                @Override
                                public boolean handleMessage(Message msg) {

                                    Log.i(TAG, "Handler what " + msg.what
                                            + " arg1 " + msg.arg1 + " arg2 "
                                            + msg.arg2);

                                    switch (msg.what) {
                                        case BixolonPrinter.MESSAGE_READ:
                                            dispatchMessage(msg);
                                            return true;
                                        case BixolonPrinter.MESSAGE_STATE_CHANGE:
                                            switch (msg.arg1) {
                                                case BixolonPrinter.STATE_CONNECTED:
                                                    mBixolonOnlineStatus = BixolonPrinter.STATE_CONNECTED;

                                                    Toast.makeText(_context,
                                                            "Printer connected",
                                                            Toast.LENGTH_LONG).show();

                                                    // Log.i(_TAG,
                                                    // " callback.run (onConnect) : "
                                                    // + current_address);

                                                    printText(" ", align, TXT_1WIDTH, TXT_1HEIGHT, false);
                                                    pixelFeed(1);

                                                    listener.onConnect();
                                                    if (LANG == 0)
                                                        dialog.setMessage("Printing");
                                                    else
                                                        dialog.setMessage("กำลังพิมพ์");

                                                    callback.run();

                                                    if (dialog != null) {
                                                        if (dialog.isShowing())
                                                            dialog.dismiss();
                                                    }

                                                    break;

                                                case BixolonPrinter.STATE_CONNECTING:

                                                    mBixolonOnlineStatus = BixolonPrinter.STATE_CONNECTING;

                                                    Toast.makeText(_context,
                                                            "Printer connecting",
                                                            Toast.LENGTH_LONG).show();

                                                    if (dialog == null) {
                                                        dialog = new ProgressDialog(
                                                                _context);
                                                    }

                                                    dialog.setCancelable(false);

                                                    if (LANG == 0)
                                                        dialog.setMessage("Connecting");
                                                    else
                                                        dialog.setMessage("กำลังเชื่อมต่อ");

                                                    dialog.show();

                                                    break;

                                                case BixolonPrinter.STATE_NONE:
                                                    // not connected
                                                    mBixolonOnlineStatus = BixolonPrinter.STATE_NONE;

                                                    if (dialog != null) {
                                                        if (dialog.isShowing())
                                                            dialog.dismiss();
                                                    }

                                                    alertCannotConnect();
                                                    Toast.makeText(_context,
                                                            "Printer not connected",
                                                            Toast.LENGTH_LONG).show();
                                                    break;
                                            }
                                            return true;
                                    }

                                    return true;
                                }
                            }), _context.getMainLooper());

                    mBixolonPrinter.automateStatusBack(true);
                    mBixolonPrinter.connect(current_address);
                } catch (Exception e) {
                    Log.i(TAG, "Bixolon Printer Error : " + e.toString());
                }

            } else {

                if (mBixolonOnlineStatus == BixolonPrinter.STATE_NONE) {

                    mBixolonPrinter.connect(current_address);

                    return;
                }

                if (mBixolonOnlineStatus == BixolonPrinter.STATE_CONNECTING) {
                    return;
                }

                // Log.i(_TAG, " callback.run : " + current_address);
                listener.onConnect();
                if (LANG == 0)
                    dialog.setMessage("Printing");
                else
                    dialog.setMessage("กำลังพิมพ์");

                callback.run();

                if (dialog != null) {
                    if (dialog.isShowing())
                        dialog.dismiss();
                }
            }

        }
    }

    public void getPairBluetoothAddress() {
        BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();

        try {
            if (adapter == null)
                adapter = BluetoothAdapter.getDefaultAdapter();

            Set<BluetoothDevice> pairedDevices = adapter.getBondedDevices();

            String currentAddress;
            for (BluetoothDevice device : pairedDevices) {

                currentAddress = device.getAddress();

                Boolean isSewooPrinter = currentAddress.indexOf(MAC_SEWOO_PATTERN) == 0;

                Boolean isBIXOLONPrinter = currentAddress.indexOf(MAC_BIXOLON_PATTERN) == 0;

                if (isBIXOLONPrinter) {
                    current_type = TYPE_BIXOLON;

                    current_address = currentAddress;
                } else if (isSewooPrinter) {
                    current_type = TYPE_SEWOO;

                    current_address = currentAddress;
                }
            }

        } catch (Exception e) {
            Log.e("Printer", e.toString());
        }
    }

    /**
     * @param text   string
     * @param align  ALIGN_XXXX
     * @param width  TXT_XWIDTH
     * @param height TXT_XHEIGHT
     * @param bold   boolean
     */
    public void printText(String text, int align, int width, int height,
                          boolean bold) {
        try {
            int attribute = 0;

            switch (getCurrent_type()) {

                case TYPE_SEWOO:

                    if (bold)
                        attribute += LKPrint.LK_FNT_BOLD;

                    mSewooPrinter.printText(text, align, attribute, width + height);
                    break;

                case TYPE_BIXOLON:

                    attribute |= BixolonPrinter.TEXT_ATTRIBUTE_FONT_A;

                    if (bold)
                        attribute |= BixolonPrinter.TEXT_ATTRIBUTE_EMPHASIZED;

                    mBixolonPrinter.printText(text, align, attribute, width
                            | height, true);
                    break;
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void printBitmap(Bitmap b, int align) {
        try {

            switch (getCurrent_type()) {

                case TYPE_SEWOO:
                    mSewooPrinter.printBitmap(b, align, LKPrint.LK_BITMAP_NORMAL);
                    break;
                case TYPE_BIXOLON:
                    mBixolonPrinter.printBitmap(b, align, 0, 50, true);
                    break;
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        b.recycle();
    }

    public void printBitmap(Bitmap b, int align, int size) {
        try {

            switch (getCurrent_type()) {

                case TYPE_SEWOO:
                    mSewooPrinter.printBitmap(b, align, size);
                    break;
                case TYPE_BIXOLON:

                    if (size == BITMAP_SIZE_DOUBLE) {
                        mBixolonPrinter.printBitmap(b, align, b.getWidth() * 2, 50,
                                true);
                    } else {
                        mBixolonPrinter.printBitmap(b, align, 0, 50, true);
                    }

                    break;
            }

            b.recycle();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void printBitmap(String pathName, int align) {
        try {

            switch (getCurrent_type()) {

                case TYPE_SEWOO:
                    mSewooPrinter.printBitmap(pathName, align,
                            LKPrint.LK_BITMAP_NORMAL);
                    break;
                case TYPE_BIXOLON:
                    mBixolonPrinter.printBitmap(pathName, align, 0, 50, true);
                    break;
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void printQRCode(String qr, int size, int QRcodeEcLevel, int align) {
        try {

            switch (getCurrent_type()) {

                case TYPE_SEWOO:
                    mSewooPrinter.printQRCode(qr, qr.length(), size, QRcodeEcLevel,
                            align);
                    break;
                case TYPE_BIXOLON:
                    mBixolonPrinter.printQrCode(qr, align,
                            BixolonPrinter.QR_CODE_MODEL2, size, true);
                    break;
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void lineFeed(int l) {

        try {
            switch (getCurrent_type()) {

                case TYPE_SEWOO:
                    mSewooPrinter.lineFeed(l);
                    break;
                case TYPE_BIXOLON:
                    mBixolonPrinter.lineFeed(l, true);
                    break;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public void pixelFeed(int l) {

        Bitmap b = Bitmap.createBitmap(1, l, Config.ARGB_4444);
        b.eraseColor(Color.WHITE);

        printBitmap(b, ALIGN_CENTER);

        b.recycle();
    }

    /**
     * not 0 = something wrong
     */
    public int getStatus() {
        int status = 0;

        switch (getCurrent_type()) {

            case TYPE_SEWOO:
                status = getSewooStatus();
                break;
            case TYPE_BIXOLON:
                status = getBIXOLONStatus();
                break;
        }

        return status;
    }

    private int getBIXOLONStatus() {

        if (mBixolonOnlineStatus == BixolonPrinter.STATE_NONE) {
            Log.e(TAG, "getBIXOLONStatus NONE");
            alertCannotConnect();
            return -1;
        }

        mBixolonPrinter.getStatus();

        return 0;
    }

    private int getSewooStatus() {
        int j = -1;
        try {
            j = mSewooPrinter.printerSts();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (j == -1) {
            Log.e(TAG, "getSewooStatus NONE");
            alertCannotConnect();

            try {
                mSewooBluetoothPort.disconnect();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return j;
        }

        if ((ESCPOSConst.LK_STS_COVER_OPEN & j) > 0) {
            alertLidOpened();

        } else if ((ESCPOSConst.LK_STS_PAPER_EMPTY & j) > 0) {
            alertNoPaper();

        } else if ((ESCPOSConst.LK_STS_BATTERY_LOW & j) > 0) {
            alertLowBattery();
        }

        if (j != 0)
            return j;

        return 0;
    }

    private void dispatchMessage(Message msg) {

        Log.i(TAG, "msg.arg1 " + msg.arg1 + " arg2 " + msg.arg2);

        switch (msg.arg1) {
            case BixolonPrinter.PROCESS_GET_STATUS:
                if (msg.arg2 == BixolonPrinter.STATUS_NORMAL) {

                } else {

                    if ((msg.arg2 & BixolonPrinter.STATUS_COVER_OPEN) == BixolonPrinter.STATUS_COVER_OPEN) {
                        alertLidOpened();

                    } else if ((msg.arg2 & BixolonPrinter.STATUS_PAPER_NOT_PRESENT) == BixolonPrinter.STATUS_PAPER_NOT_PRESENT) {
                        alertNoPaper();


                    } else if (BixolonPrinter.STATUS_COVER_OPEN == (BixolonPrinter.STATUS_BATTERY_LOW & msg.arg2)) {
                        alertLowBattery();
                    }

                }

                break;
        }
    }

    private AlertDialog.Builder builder1;
    private AlertDialog alert;

    private void showdialog(String message) {
        if (alert != null && alert.isShowing()) {
            alert.dismiss();
            alert.cancel();
        }
        builder1 = new AlertDialog.Builder(_context);
        builder1.setMessage(message);
        builder1.setCancelable(false);

        builder1.setNeutralButton(android.R.string.ok,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        alert = builder1.create();
        alert.show();
    }

    public void dispose() {
        if ((hThread != null) && (hThread.isAlive())) {
            hThread.interrupt();
            hThread = null;
        }
    }

    private void alertCannotConnect() {
        listener.onDisconnect();
        if (LANG == 0)
            showdialog("Unable to connect to printer");
        else
            showdialog("เครื่องปริ้นติดต่อไม่ได้ ");
    }

    private void alertLidOpened() {
        listener.onDisconnect();
        if (LANG == 0)
            showdialog("Printer cover is not completely closed.\nPlease close the cover tightly.");
        else
            showdialog("ฝาเครื่องปริ้นท์ปิดไม่สนิท\nกรุณาปิดฝาเครื่องปริ้นท์ให้สนิทค่ะ");
    }

    private void alertNoPaper() {
        listener.onDisconnect();
        if (LANG == 0)
            showdialog("Printer runs out of paper.\nPlease replace a new roll of paper and try again.");
        else
            showdialog("กระดาษเครื่องปริ้นท์หมด\nโปรดใส่ม้วนกระดาษใหม่ แล้วลองอีกครั้งค่ะ");
    }

    private void alertLowBattery() {
        listener.onDisconnect();
        if (LANG == 0)
            showdialog("Printer's low battery.\nPlease recharge.");
        else
            showdialog("แบ็ตเครื่องปริ้นท์อ่อน\nกรุณาชาร์จแบตค่ะ");
    }
}
