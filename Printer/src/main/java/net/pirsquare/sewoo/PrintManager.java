package net.pirsquare.sewoo;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Handler;
import android.text.Layout.Alignment;
import android.text.StaticLayout;
import android.text.TextPaint;

import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.sewoo.jpos.printer.LKPrint;

import net.glxn.qrgen.android.QRCode;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class PrintManager {
    public static final String TAG = "PRINTER MANAGER";

    private Context _context;
    private static String _lastConnAddr;
    private Printer _printManager;
    private PrinterListener printerListener;

    public PrintManager(Context _context, PrinterListener listener) {
        this._context = _context;
        this.printerListener = listener;
    }

    public void setAddress(String lastConnAddr) {
        _lastConnAddr = lastConnAddr;

        if (_printManager != null)
            _printManager.current_address = _lastConnAddr;
    }


    public void printQueue(final int type,
                           final String time_stamp, final String queue_id,
                           final String seat_amount, final String qr, final int queue_remain, String refCode) {

        if (_printManager == null) {
            _printManager = new Printer(_context, _lastConnAddr, printerListener);
        }


        if (_printManager.getStatus() == 0) {
            print(type, time_stamp, queue_id, seat_amount.equalsIgnoreCase("0 Seats") ? "1 Seats" : seat_amount, qr,
                    queue_remain, refCode);
        }
//        _printManager.preparePrint(new Runnable() {
//            @Override
//            public void run() {
//                int status = _printManager.getStatus();
//                if (status == 0) {
//                    print(type, time_stamp, queue_id, seat_amount.equalsIgnoreCase("0 Seats") ? "1 Seats" : seat_amount, qr,
//                            queue_remain);
//                }
//            }
//        }, Printer.ALIGN_CENTER);
    }

    public void printReceipt(final String take_away_data) {


        // Log.i(TAG, " printQueue : " + _lastConnAddr);

        if (_printManager == null) {
            _printManager = new Printer(_context, _lastConnAddr, printerListener);
        }

        _printManager.preparePrint(new Runnable() {

            @Override
            public void run() {
                // Log.i(TAG, " onCallback");
                int status = _printManager.getStatus();

                // Log.i(TAG, "first status check : " + status);
                if (status == 0) {
                    printReceipted(take_away_data);
                }
            }

        }, Printer.ALIGN_CENTER);
    }

    // init
    // ==================================================================================================================


    // print
    // ==================================================================================================================

    protected void print(final int type, final String time_stamp,
                         final String queue_id, final String seat_amount, final String qr,
                         final int queue_remain, String refCode) {

//        if (dialog == null) {
//            dialog = new ProgressDialog(_context);
//        }
//
//        dialog.setCancelable(false);
//        dialog.setMessage("Printing\nกำลังปริ้น");
//        dialog.show();
//
//        new Handler(_context.getMainLooper()).postDelayed(new Runnable() {
//
//            @Override
//            public void run() {
//                proceeedPrint(type, time_stamp, queue_id, seat_amount, qr,
//                        queue_remain);
//            }
//        }, Printer.DELAY_DIALOGUE);
        new Printing(type, time_stamp, queue_id, seat_amount, qr, queue_remain, refCode).execute();
    }

    private class Printing extends AsyncTask<String, Void, String> {
        private ProgressDialog dialogloading;

        private int type;
        private String time_stamp;
        private String queue_id;
        private String seat_amount;
        private String qr;
        private int queue_remain;
        private String refCode;

        public Printing(int type, String time_stamp, String queue_id, String seat_amount, String qr, int queue_remain, String refCode) {
            this.type = type;
            this.time_stamp = time_stamp;
            this.queue_id = queue_id;
            this.seat_amount = seat_amount;
            this.qr = qr;
            this.queue_remain = queue_remain;
            this.refCode = refCode;
        }

        protected String doInBackground(String... params) {
            boolean result = proceeedPrint(type, time_stamp, queue_id, seat_amount, qr,
                    queue_remain, refCode);
            return result ? "yes" : "no";
        }

        protected void onPostExecute(String result) {
            this.dialogloading.dismiss();

        }

        protected void onPreExecute() {
            if (dialogloading == null)
                this.dialogloading = new ProgressDialog(_context);
            this.dialogloading.dismiss();
            this.dialogloading.setProgressStyle(0);
            this.dialogloading.setMessage("Please wait. / กำลังปริ้น");
            this.dialogloading.setIndeterminate(true);
            this.dialogloading.setCanceledOnTouchOutside(false);
            this.dialogloading.show();
        }
    }


    public void checkPrint(CheckPrintListener listener) {
        if (_printManager == null) {
            _printManager = new Printer(_context, _lastConnAddr, printerListener);
            listener.onCheckSuccess(_printManager.current_address);
        }

        _printManager.preparePrint(new Runnable() {
            @Override
            public void run() {
                int status = _printManager.getStatus();
                if (status == 0) {
                    new Handler(_context.getMainLooper()).postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            testPrint("");

                        }
                    }, Printer.DELAY_DIALOGUE);
                }
            }
        }, Printer.ALIGN_CENTER);
    }

    private void testPrint(String macAddress) {
        _printManager.printText("\n" + macAddress + "\n",
                Printer.ALIGN_CENTER, Printer.TXT_1WIDTH,
                Printer.TXT_1HEIGHT, false);

        _printManager.pixelFeed(10);

        if (_printManager.getCurrent_type() == Printer.TYPE_SEWOO)
            _printManager.lineFeed(1);
    }

    private boolean proceeedPrint(final int type, final String time_stamp,
                                  final String queue_id, final String seat_amount, final String qr,
                                  final int queue_remain, String refCode) {
        if (type == 0) {
            // top-logo
            // -----------------------------------------------------------------------------
            printNormalTopLogo();
            // middle-text
            // --------------------------------------------------To---------------------------
            _printManager.printText("\n" + time_stamp + "\n",
                    Printer.ALIGN_CENTER, Printer.TXT_1WIDTH,
                    Printer.TXT_1HEIGHT, true);

            _printManager.printText("\n" + queue_id + "\n",
                    Printer.ALIGN_CENTER, Printer.TXT_4WIDTH,
                    Printer.TXT_3HEIGHT, false);

            _printManager.printText("\n" + seat_amount + "\n",
                    Printer.ALIGN_CENTER, Printer.TXT_1WIDTH,
                    Printer.TXT_1HEIGHT, true);

            // qr
            // -----------------------------------------------------------------------------
            printNormalQR(qr);

            // ////////////////version 2
        } else if (type == 1) {
            // top-logo
            // -----------------------------------------------------------------------------
            printAdvanceTopLogoWithTimeStamp(time_stamp);

            // qr
            // -----------------------------------------------------------------------------
            printAdvanceQRQueueIDAndSeatCount(qr, queue_id, seat_amount);
            // ////////////version 3
        } else if (type == 2) {
            // top-logo
            // -----------------------------------------------------------------------------
            printAdvanceTopLogoWithTimeStamp(time_stamp);

            // middle-text
            // --------------------------------------------------To---------------------------
            // Printer
            _printManager.printText("\n" + queue_id + "\n",
                    Printer.ALIGN_CENTER, Printer.TXT_4WIDTH,
                    Printer.TXT_3HEIGHT, false);

            _printManager.printText("\n" + seat_amount + "\n",
                    Printer.ALIGN_CENTER, Printer.TXT_1WIDTH,
                    Printer.TXT_1HEIGHT, true);

            // qr
            // -----------------------------------------------------------------------------
            printNormalQR(qr);
            // ///////////////version 4
        } else if (type == 3) {
            // top-logo
            // -----------------------------------------------------------------------------
            printNormalTopLogo();
            //

            if (_printManager.getCurrent_type() == Printer.TYPE_SEWOO)
                _printManager.lineFeed(1);

            _printManager.printText(time_stamp + "\n",
                    Printer.ALIGN_CENTER, Printer.TXT_1WIDTH,
                    Printer.TXT_1HEIGHT, true);

            // // qr
            // //
            // -----------------------------------------------------------------------------
            printAdvanceQRQueueIDAndSeatCount(qr, queue_id, seat_amount);
        }
        // bottom-logo
        // -----------------------------------------------------------------------------

        printRemainBitmap(queue_remain);

        if (_printManager.getCurrent_type() == Printer.TYPE_SEWOO)
            _printManager.lineFeed(1);
        _printManager.printText("LINE Code : @" + refCode + "\n\n",
                Printer.ALIGN_CENTER, Printer.TXT_1WIDTH,
                Printer.TXT_1HEIGHT, false);

        _printManager.printBitmap("//sdcard//logo_bottom.jpg",
                Printer.ALIGN_CENTER);

        _printManager.pixelFeed(55);

        if (_printManager.getCurrent_type() == Printer.TYPE_SEWOO)
            _printManager.lineFeed(1);

        int status = _printManager.getStatus();
        return true;
    }

    private static final Config canvas_config = Config.ARGB_4444;
    // max width hardware can support
    // private static final int canvas_max_width = Printer.MAX_WIDTH;

    private static Bitmap _top_canvas_bitmap;
    private static Bitmap _remain_bitmap;

    private static int[] _top_logo_bitmap_int_data;
    private static int[] _top_logo_bitmap_width_height;
    private static final int _qr_bitmap_size = 90;
    private static final int _queue_id_top_margin = 1;
    private static final int _seat_bottom_margin = 6;

    public static int REMAIN_FONT_SIZE = 26;
    public static int REMAIN_FONT_AREA = 67;

    private void createBitmapCache() {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = canvas_config;

        Bitmap bitmap = BitmapFactory.decodeFile("//sdcard//logo_top.jpg",
                options);

        _top_logo_bitmap_width_height = new int[]{bitmap.getWidth(),
                bitmap.getHeight()};
        _top_logo_bitmap_int_data = new int[_top_logo_bitmap_width_height[0]
                * _top_logo_bitmap_width_height[1]];
        bitmap.getPixels(_top_logo_bitmap_int_data, 0,
                _top_logo_bitmap_width_height[0], 0, 0,
                _top_logo_bitmap_width_height[0],
                _top_logo_bitmap_width_height[1]);

        // bitmap.recycle();
        // bitmap = null;

        _top_canvas_bitmap = Bitmap.createBitmap(Printer.MAX_WIDTH,
                _top_logo_bitmap_width_height[1], canvas_config);

    }

    /**
     * put logo on left and time stamp on right
     *
     * @param time_stamp
     */
    private void applyTopBitmap(String time_stamp) {
        _top_canvas_bitmap.eraseColor(Color.WHITE);

        // set pixels for time on right
        // get bitmap from string
        Bitmap text_bitmap = textAsBitmap(time_stamp, 25,
                Alignment.ALIGN_NORMAL, Printer.MAX_WIDTH);
        // get pixel
        int[] text_bitmap_width_height = {text_bitmap.getWidth(),
                text_bitmap.getHeight()};
        int[] text_bitmap_int_data = new int[text_bitmap_width_height[0]
                * text_bitmap_width_height[1]];
        text_bitmap.getPixels(text_bitmap_int_data, 0,
                text_bitmap_width_height[0], 0, 0, text_bitmap_width_height[0],
                text_bitmap_width_height[1]);

        int center_y = (int) (_top_logo_bitmap_width_height[1] * .5 - text_bitmap_width_height[1] * .5);
        int middle_divider = Printer.MAX_WIDTH - _qr_bitmap_size * 2;
        _top_canvas_bitmap.setPixels(text_bitmap_int_data, 0,
                text_bitmap_width_height[0], middle_divider, center_y,
                text_bitmap_width_height[0], text_bitmap_width_height[1]);

        // calculate centering left block
        int top_left_spacing = (int) (middle_divider * .5 - _top_logo_bitmap_width_height[0] * .5);
        // set pixels for logo on left
        _top_canvas_bitmap.setPixels(_top_logo_bitmap_int_data, 0,
                _top_logo_bitmap_width_height[0], top_left_spacing, 0,
                _top_logo_bitmap_width_height[0],
                _top_logo_bitmap_width_height[1]);

        // gc not use
        text_bitmap_width_height = null;
        text_bitmap_int_data = null;

        if (text_bitmap != null)
            text_bitmap.recycle();
        text_bitmap = null;
    }

    private Bitmap orderLine(String menu, String price) {

        Bitmap price_bm = textAsBitmap(price, 24, Alignment.ALIGN_OPPOSITE,
                Printer.MAX_WIDTH);
        Bitmap menu_bm = textAsBitmap(menu, 24, Alignment.ALIGN_NORMAL,
                Printer.MAX_WIDTH - price_bm.getWidth());

        int height1 = menu_bm.getHeight();
        int height2 = price_bm.getHeight();

        int max_height = height1 > height2 ? height1 : height2;

        Bitmap result_bm = Bitmap.createBitmap(Printer.MAX_WIDTH,
                max_height, canvas_config);

        // copy price to bm
        int[] price_bm_width_height = {price_bm.getWidth(),
                price_bm.getHeight()};
        int[] price_bm_int_data = new int[price_bm_width_height[0]
                * price_bm_width_height[1]];

        price_bm.getPixels(price_bm_int_data, 0, price_bm_width_height[0], 0,
                0, price_bm_width_height[0], price_bm_width_height[1]);

        int final_price_width = price_bm_width_height[0] > Printer.MAX_WIDTH ? Printer.MAX_WIDTH
                : price_bm_width_height[0];

        result_bm.eraseColor(Color.WHITE);

        result_bm.setPixels(price_bm_int_data, 0, price_bm_width_height[0],
                Printer.MAX_WIDTH - final_price_width,// x
                0,// y
                final_price_width, price_bm_width_height[1]);

        // copy menu to bm
        int[] menu_bm_width_height = {menu_bm.getWidth(), menu_bm.getHeight()};
        int[] menu_bm_int_data = new int[menu_bm_width_height[0]
                * menu_bm_width_height[1]];

        menu_bm.getPixels(menu_bm_int_data, 0, menu_bm_width_height[0], 0, 0,
                menu_bm_width_height[0], menu_bm_width_height[1]);

        int menu_price_width_final = menu_bm_width_height[0] > Printer.MAX_WIDTH ? Printer.MAX_WIDTH
                : menu_bm_width_height[0];
        result_bm.setPixels(menu_bm_int_data, 0, menu_bm_width_height[0], 0,// x
                0,// y
                menu_price_width_final, menu_bm_width_height[1]);

        return result_bm;

    }

    private Bitmap textAsBitmap(String text, float textSize, Alignment allign,
                                int maxWidth) {
        // Get text dimensions

        return textAsBitmapBox(text, textSize, allign, maxWidth, -1);
    }

    private Bitmap textAsBitmapBox(String text, float textSize,
                                   Alignment allign, int maxWidth, int boxWidth) {
        // Get text dimensions
        TextPaint textPaint = new TextPaint(Paint.FILTER_BITMAP_FLAG
                | Paint.LINEAR_TEXT_FLAG | Paint.FAKE_BOLD_TEXT_FLAG);
        textPaint.setStyle(Paint.Style.FILL);
        textPaint.setColor(Color.BLACK);
        textPaint.setTextSize(textSize);
        textPaint.measureText(text);
        int width = boxWidth == -1 ? ((int) textPaint.measureText(text) + 2)
                : boxWidth;

        StaticLayout mTextLayout = new StaticLayout(text, textPaint,
                width > maxWidth ? maxWidth : width, allign, 1.0f, 0.0f, false);

        // Create bitmap and canvas to draw to
        Bitmap b = Bitmap.createBitmap(mTextLayout.getWidth(),
                mTextLayout.getHeight(), canvas_config);
        Canvas c = new Canvas(b);

        // Draw background
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.LINEAR_TEXT_FLAG);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.WHITE);
        c.drawPaint(paint);

        // Draw text
        c.save();
        c.translate(0, 0);
        mTextLayout.draw(c);
        c.restore();

        return b;
    }

    private Bitmap applyMiddleBitmap(String qr, String queue_id,
                                     String seat_amount) {
        Bitmap middle_canvas_bitmap = Bitmap.createBitmap(
                (int) (Printer.MAX_WIDTH * .5), _qr_bitmap_size,
                canvas_config);
        middle_canvas_bitmap.eraseColor(Color.WHITE);

        // default no qr
        int qr_x_pos = middle_canvas_bitmap.getWidth();
        int[] qr_bitmap_width_height;
        int[] qr_bitmap_int_data;
        Bitmap qr_bitmap = null;
        if (!qr.isEmpty()) {
            // get pixels for qr
            qr_bitmap = QRCode.from(qr)
                    .withErrorCorrection(ErrorCorrectionLevel.L)
                    .withSize(_qr_bitmap_size, _qr_bitmap_size).bitmap();
            qr_bitmap_width_height = new int[]{qr_bitmap.getWidth(),
                    qr_bitmap.getHeight()};
            qr_bitmap_int_data = new int[qr_bitmap_width_height[0]
                    * qr_bitmap_width_height[1]];
            qr_bitmap.getPixels(qr_bitmap_int_data, 0,
                    qr_bitmap_width_height[0], 0, 0, qr_bitmap_width_height[0],
                    qr_bitmap_width_height[1]);

            // put qr at right
            qr_x_pos = middle_canvas_bitmap.getWidth()
                    - qr_bitmap_width_height[0];
            middle_canvas_bitmap
                    .setPixels(
                            qr_bitmap_int_data,
                            0,
                            qr_bitmap_width_height[0],
                            qr_x_pos,
                            (int) (middle_canvas_bitmap.getHeight() * .5 - qr_bitmap_width_height[1] * .5),
                            qr_bitmap_width_height[0],
                            qr_bitmap_width_height[1]);
        }

        int remain_left_area = qr_x_pos;

        // get bitmap for queue_id
        Bitmap queue_id_bitmap = textAsBitmapBox(queue_id, 43,
                Alignment.ALIGN_CENTER, Printer.MAX_WIDTH,
                remain_left_area);
        // get pixels for queue_id
        int[] queue_id_bitmap_width_height = {queue_id_bitmap.getWidth(),
                queue_id_bitmap.getHeight()};
        int[] queue_id_bitmap_int_data = new int[queue_id_bitmap_width_height[0]
                * queue_id_bitmap_width_height[1]];
        queue_id_bitmap.getPixels(queue_id_bitmap_int_data, 0,
                queue_id_bitmap_width_height[0], 0, 0,
                queue_id_bitmap_width_height[0],
                queue_id_bitmap_width_height[1]);

        // put queue_id on top left
        middle_canvas_bitmap.setPixels(queue_id_bitmap_int_data, 0,
                queue_id_bitmap_width_height[0], 0, _queue_id_top_margin,
                queue_id_bitmap_width_height[0],
                queue_id_bitmap_width_height[1]);

        // get bitmap for seat_amount
        Bitmap seat_amount_bitmap = textAsBitmapBox(seat_amount.equalsIgnoreCase("0") ? "1" : seat_amount, 20,
                Alignment.ALIGN_CENTER, Printer.MAX_WIDTH,
                remain_left_area);
        // get pixels for seat_amount
        int[] seat_amount_bitmap_width_height = {
                seat_amount_bitmap.getWidth(), seat_amount_bitmap.getHeight()};
        int[] seat_amount_bitmap_int_data = new int[seat_amount_bitmap_width_height[0]
                * seat_amount_bitmap_width_height[1]];
        seat_amount_bitmap.getPixels(seat_amount_bitmap_int_data, 0,
                seat_amount_bitmap_width_height[0], 0, 0,
                seat_amount_bitmap_width_height[0],
                seat_amount_bitmap_width_height[1]);

        // int seat_amount_y_center = (int) ((middle_canvas_bitmap.getHeight() +
        // _queue_id_top_margin - queue_id_bitmap_width_height[1] -
        // _seat_bottom_margin) * .5 - (seat_amount_bitmap_width_height[1] +
        // _seat_bottom_margin) * .5 + queue_id_bitmap_width_height[1]);
        int seat_amount_y_center = middle_canvas_bitmap.getHeight()
                - seat_amount_bitmap_width_height[1] - _seat_bottom_margin;

        // put seat_amount on bottom left
        middle_canvas_bitmap.setPixels(seat_amount_bitmap_int_data, 0,
                seat_amount_bitmap_width_height[0], 0, seat_amount_y_center,
                seat_amount_bitmap_width_height[0],
                seat_amount_bitmap_width_height[1]);

        // gc not use
        if (qr_bitmap != null)
            qr_bitmap.recycle();
        qr_bitmap = null;
        qr_bitmap_width_height = null;
        qr_bitmap_int_data = null;
        // gc not use
        if (queue_id_bitmap != null)
            queue_id_bitmap.recycle();

        if (queue_id_bitmap != null)
            queue_id_bitmap.recycle();

        queue_id_bitmap = null;
        queue_id_bitmap_width_height = null;
        queue_id_bitmap_int_data = null;
        // gc not use

        if (seat_amount_bitmap != null)
            seat_amount_bitmap.recycle();

        seat_amount_bitmap = null;
        seat_amount_bitmap_width_height = null;
        seat_amount_bitmap_int_data = null;

        return middle_canvas_bitmap;
    }

    private void printRemainBitmap(int queue_remain) {
        if (queue_remain <= 0)
            return;

        try {
            _printManager.printBitmap(
                    textAsBitmap((Printer.LANG == 0 ? "Queue to wait "
                                    : "คุณเป็นคิวที่  ") + queue_remain,
                            REMAIN_FONT_SIZE, Alignment.ALIGN_NORMAL,
                            Printer.MAX_WIDTH), Printer.ALIGN_CENTER);
            // if (_remain_bitmap == null
            // && new File("//sdcard//remain_msg.png").exists()) {
            //
            // BitmapFactory.Options options = new BitmapFactory.Options();
            // options.inPreferredConfig = canvas_config;
            // options.inMutable = true;
            // _remain_bitmap = BitmapFactory.decodeFile(
            // "//sdcard//remain_msg.png", options);
            // }
            //
            // if (queue_remain > 999) {
            // queue_remain = 999;
            // }
            //
            // Bitmap number_bitmap = textAsBitmap(queue_remain + "",
            // REMAIN_FONT_SIZE, Alignment.ALIGN_CENTER,
            // Printer.MAX_WIDTH);
            //
            // // get pixels for queue_id
            // int[] number_bitmap_width_height = { number_bitmap.getWidth(),
            // number_bitmap.getHeight() };
            //
            // int[] queue_id_bitmap_int_data = new
            // int[number_bitmap_width_height[0]
            // * number_bitmap_width_height[1]];
            //
            // number_bitmap.getPixels(queue_id_bitmap_int_data, 0,
            // number_bitmap_width_height[0], 0, 0,
            // number_bitmap_width_height[0],
            // number_bitmap_width_height[1]);
            //
            // _remain_bitmap
            // .setPixels(
            // queue_id_bitmap_int_data,
            // 0,
            // number_bitmap_width_height[0],
            // _remain_bitmap.getWidth()
            // - number_bitmap_width_height[0],
            // (int) (_remain_bitmap.getHeight() * .5 -
            // number_bitmap_width_height[1] * .5),
            // number_bitmap_width_height[0],
            // number_bitmap_width_height[1]);
            //
            // _printManager
            // .printBitmap(_remain_bitmap, Printer.ALIGN_CENTER);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void printNormalQR(String qr) {
        try {
            if (!qr.isEmpty()) {
                _printManager
                        .printText("\n    ", Printer.ALIGN_CENTER,
                                Printer.TXT_1WIDTH,
                                Printer.TXT_1HEIGHT, true);

                _printManager.printQRCode(qr, 4, LKPrint.LK_QRCODE_EC_LEVEL_L,
                        LKPrint.LK_ALIGNMENT_CENTER);

                _printManager
                        .printText("\n", Printer.ALIGN_CENTER,
                                Printer.TXT_1WIDTH,
                                Printer.TXT_1HEIGHT, true);
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    private void printAdvanceQRQueueIDAndSeatCount(String qr, String queue_id,
                                                   String seat_amount) {
        Bitmap b = applyMiddleBitmap(qr, queue_id, seat_amount);
        _printManager.printBitmap(b, Printer.ALIGN_CENTER,
                Printer.BITMAP_SIZE_DOUBLE);

        // b.recycle();
        b = null;
    }

    private void printNormalTopLogo() {
        _printManager.printBitmap("//sdcard//logo_top.jpg",
                Printer.ALIGN_CENTER);
    }

    private void printAdvanceTopLogoWithTimeStamp(String time_stamp) {
        if (_top_canvas_bitmap == null)
            createBitmapCache();

        applyTopBitmap(time_stamp);
        _printManager.printBitmap(_top_canvas_bitmap, Printer.ALIGN_LEFT,
                Printer.BITMAP_SIZE_NORMAL);
    }

    private void printReceipted(String take_away_data) {

        String card_id = "";
        String name = "Guest";
        Date date = null;
        String provider = "omese";
        String queue_id = null;
        JSONArray cart_summary = null;

        try {
            JSONObject json_object = new JSONObject(take_away_data);

            if (json_object.has("provider")) {
                provider = json_object.getString("provider");
            }

            if (json_object.has("queue_id")) {
                queue_id = json_object.getString("queue_id");
            }

            if (json_object.has("name")) {
                name = json_object.getString("name");
            }

            if (json_object.has("card")) {
                card_id = json_object.getString("card");
            }

            if (json_object.has("createdAt")) {

                String date_String = json_object.getString("createdAt");// 2016-01-25T04:44:37.323Z//"2016-01-29T06:01:49-05:00"
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ",
                        Locale.ENGLISH);
                try {
                    date = df.parse(date_String);
                } catch (ParseException e1) {
                    e1.printStackTrace();
                }
            }

            if (json_object.has("cart_summary")) {
                cart_summary = json_object.getJSONArray("cart_summary");
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        // TODO Auto-generated method stub
        _printManager.printBitmap("//sdcard//logo_top.jpg",
                Printer.ALIGN_CENTER);

        _printManager.pixelFeed(10);

        if ((name + "").equals("null")) {
            name = provider;
        }

        _printManager.printBitmap(
                textAsBitmap((Printer.LANG == 0 ? "Paid by " : "จ่ายโดย ")
                                + name, 24, Alignment.ALIGN_NORMAL,
                        Printer.MAX_WIDTH), Printer.ALIGN_CENTER);

        _printManager.pixelFeed(5);

        if ((card_id + "").length() == 4) {
            _printManager.printBitmap(
                    textAsBitmap((Printer.LANG == 0 ? "Card " : "เลขบัตร "
                                    + "XXXX-XXXX-XXXX-")
                                    + card_id, 26, Alignment.ALIGN_NORMAL,
                            Printer.MAX_WIDTH), Printer.ALIGN_CENTER);
        }

        _printManager.pixelFeed(5);
        String vv = new SimpleDateFormat("dd MMM yy HH:mm:ss").format(date);

        _printManager.printBitmap(
                textAsBitmap(
                        (Printer.LANG == 0 ? "Time " : "เวลา  ") + vv, 24,
                        Alignment.ALIGN_NORMAL, Printer.MAX_WIDTH),
                Printer.ALIGN_CENTER);
        _printManager.pixelFeed(10);

        _printManager.printBitmap(
                textAsBitmap((Printer.LANG == 0 ? "Take Away Orders"
                                : "รายการสั่งกลับบ้าน"), 40, Alignment.ALIGN_NORMAL,
                        Printer.MAX_WIDTH), Printer.ALIGN_CENTER);
        _printManager.pixelFeed(5);

        int cart_items = cart_summary.length();
        int total = 0;

        for (int i = 0; i < cart_items; i++) {
            try {
                JSONObject item = cart_summary.getJSONObject(i);

                String title = item.getString("title");
                int count = item.getInt("count");
                int price = item.getInt("price");

                total += price * count;

                _printManager.printBitmap(
                        orderLine(title, " (" + count + ") " + price + " บาท"),
                        Printer.ALIGN_CENTER);
                _printManager.pixelFeed(5);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        _printManager.printBitmap(
                orderLine("Sub total : ", " " + total + " บาท"),
                Printer.ALIGN_LEFT);

        if (queue_id != null && queue_id.length() > 0) {
            _printManager.pixelFeed(10);
            _printManager.printBitmap(
                    textAsBitmap(queue_id, 60, Alignment.ALIGN_NORMAL,
                            Printer.MAX_WIDTH), Printer.ALIGN_CENTER);
            _printManager.pixelFeed(10);
        }

        _printManager.pixelFeed(14);

        _printManager.printBitmap("//sdcard//logo_bottom.jpg",
                Printer.ALIGN_CENTER);

        _printManager.pixelFeed(55);

        if (_printManager.getCurrent_type() == Printer.TYPE_SEWOO)
            _printManager.lineFeed(1);
    }


    //@Override
    public void dispose() {
        // TODO Auto-generated method stub
        _printManager.dispose();
    }

    public interface CheckPrintListener {
        void onCheckSuccess(String macAddr);

        void onFail();
    }

    public interface PrinterListener {
        void onConnect();

        void onDisconnect();
    }
}
